import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:lookinservice/Views/notifications_page.dart';
import 'package:lookinservice/Views/success_page.dart';
import 'Views/Auth/branch_registration.dart';
import 'Views/Auth/login_page.dart';
import 'Views/Auth/map_page.dart';
import 'Views/Auth/restore_password.dart';
import 'Views/Auth/trade_mark_registration.dart';
import 'Views/More/change_password.dart';
import 'Views/More/edit_profile_page.dart';
import 'Views/Messages/messages_page.dart';
import 'Views/Settings/reject_request_page.dart';
import 'Views/Settings/add_reject_request_page.dart';
import 'Views/Settings/settings_page.dart';
import 'Views/StartPages/splash_screen.dart';
import 'Views/More/ratings_page.dart';
import 'Views/More/about_us_page.dart';
import 'Views/Settings/change_language_page.dart';
import 'Views/More/contact_us_page.dart';
import 'Views/Settings/privacy_policy_page.dart';
import 'Views/Settings/terms_conditions_page.dart';
import 'Views/StartPages/language_page.dart';
import 'Widgets/dashboard.dart';
import 'PreDefined/localization.dart';
import 'PreDefined/app_constants.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  runApp(
      // DevicePreview(builder: (context) => MyApp(),enabled: !kReleaseMode,)
      MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  Locale _locale;
  //Initial value before any selection is made

  final routes = <String, WidgetBuilder>{
    Dashboard.tag: (context) => Dashboard(),
    LanguagePage.tag: (context) => LanguagePage(),
    LanguagePage.tag: (context) => LanguagePage(),
    AboutUsPage.tag: (context) => AboutUsPage(),
    ContactUsPage.tag: (context) => ContactUsPage(),
    MessagesPage.tag: (context) => MessagesPage(),
    NotificationsPage.tag: (context) => NotificationsPage(),
    PrivacyPolicyPage.tag: (context) => PrivacyPolicyPage(),
    TermsConditionsPage.tag: (context) => TermsConditionsPage(),
    EditProfilePage.tag: (context) => EditProfilePage(),
    ChangeLanguagePage.tag: (context) => ChangeLanguagePage(),
    RatingsPage.tag: (context) => RatingsPage(),
    SplashScreenPage.tag: (context) => SplashScreenPage(),
    ChangePasswordPage.tag: (context) => ChangePasswordPage(),
    SettingsPage.tag: (context) => SettingsPage(),
    RejectRequestReasons.tag: (context) => RejectRequestReasons(),
    AddRejectRequestReasons.tag: (context) => AddRejectRequestReasons(),
    RatingsPage.tag: (context) => RatingsPage(),
    LoginPage.tag: (context) => LoginPage(),
    RestorePasswordPage.tag: (context) => RestorePasswordPage(),
    TradeMarkRegistration.tag: (context) => TradeMarkRegistration(),
    SuccessPage.tag: (context) => SuccessPage(),
    BranchRegistrationPage.tag: (context) => BranchRegistrationPage(),
    MapPage.tag: (context) => MapPage(),
  };

  @override
  void initState() {
    super.initState();
    _locale = appLanguageId == 1 ? Locale('en') : Locale('ar');
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'lookinService',
      theme: ThemeData(
        primaryColor: primaryColor,
        accentColor: accentColor,
        fontFamily: 'Tajawal',
        cursorColor: primaryColor,
        textSelectionColor: primaryColor,
        textSelectionHandleColor: primaryColor,
      ),
      debugShowCheckedModeBanner: false,
      home: SplashScreenPage(),
      localizationsDelegates: [
        const AppLocalizationsDelegate(),
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      supportedLocales: [
        const Locale('en', ''),
        const Locale('ar', ''),
      ],
      routes: routes,
      locale: _locale,
    );
  }
}
