import 'package:flutter/material.dart';
import 'package:lookinservice/PreDefined/localization.dart';
import 'package:lookinservice/Widgets/text_widgets.dart';

import '../PreDefined/app_constants.dart';

typedef BottomTabsCallback = void Function(int index);

class AppBottomBar extends StatefulWidget {
  final BottomTabsCallback onBottomTabSelect;
  final int currentbarIndex;

  AppBottomBar({
    @required this.onBottomTabSelect,
    @required this.currentbarIndex,
  });

  @override
  _AppBottomBarState createState() => _AppBottomBarState();
}

class _AppBottomBarState extends State<AppBottomBar> {
  AppLocalizations appLocalizations;
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    appLocalizations = AppLocalizations.of(context);

    return Directionality(
      textDirection: isRightToLeft ? TextDirection.rtl : TextDirection.ltr,
      child: Container(
        child: BottomNavigationBar(
          //backgroundColor: Theme.of(context).primaryColor,
          type: BottomNavigationBarType.fixed,
          onTap: widget.onBottomTabSelect,
          currentIndex: widget.currentbarIndex,
          //fixedColor: Theme.of(context).accentColor,
          items: [
            BottomNavigationBarItem(
              activeIcon: Image.asset(
                'assets/home.png',
                height: 30,
                width: 40,
              ),
              title: MyText.sText(
                context: context,
                text: 'Home',
              ),
              icon: Image.asset(
                'assets/home-in.png',
                height: 30,
                width: 40,
              ),
            ),
            BottomNavigationBarItem(
              activeIcon: Image.asset(
                'assets/reservation.png',
                height: 30,
                width: 40,
              ),
              title: MyText.sText(
                context: context,
                text: 'Bookings',
              ),
              icon: Image.asset(
                'assets/reservation-in.png',
                height: 30,
                width: 40,
              ),
            ),
          BottomNavigationBarItem(
              activeIcon: Image.asset(
                'assets/messages.png',
                height: 30,
                width: 40,
              ),
              title: MyText.sText(
                context: context,
                text: 'messages',
              ),
              icon: Image.asset(
                'assets/messages-in.png',
                height: 30,
                width: 40,
              ),
            ),
            // BottomNavigationBarItem(
            //   activeIcon: Image.asset(
            //     'assets/notification.png',
            //     height: 30,
            //     width: 40,
            //   ),
            //   title: MyText.sText(
            //     context: context,
            //     text: 'notifications',
            //   ),
            //   icon: Image.asset(
            //     'assets/notification-in.png',
            //     height: 30,
            //     width: 40,
            //   ),
            // ),
            BottomNavigationBarItem(
              activeIcon: Image.asset(
                'assets/more.png',
                height: 30,
                width: 40,
              ),
              title: MyText.sText(
                context: context,
                text: 'More',
              ),
              icon: Image.asset(
                'assets/more-in.png',
                height: 30,
                width: 40,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
