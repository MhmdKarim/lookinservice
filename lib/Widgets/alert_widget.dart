import 'dart:ui';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:lookinservice/PreDefined/app_constants.dart';
import 'package:lookinservice/PreDefined/localization.dart';
import 'package:lookinservice/Widgets/text_widgets.dart';

class MyPopup{
static  showPopUp(
    {BuildContext context, Function function, String title, String message}) {
  showDialog(
    context: context,
    barrierDismissible: true,
    builder: (BuildContext context) {
      return Directionality(
        textDirection: isRightToLeft ? TextDirection.rtl : TextDirection.ltr,
        child: SimpleDialog(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(10),
              child: Text(
                title,
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: Text(
                message,
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 25,
                ),
              ),
            ),
            Container(
              height: 1,
              width: double.infinity,
              color: accentColor,
            ),
            Container(
              height: 40,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  FlatButton(
                    onPressed: function,
                    child: MyText.lText(
                        context: context, text: 'yes', color: primaryColor),
                  ),
                  Container(
                    width: 1,
                    height: 60,
                    color: accentColor,
                  ),
                  Container(
                    width: 100,
                    child: FlatButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: MyText.lText(
                          context: context, text: 'no', color: primaryColor),
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      );
    },
  );
}

static   Future<bool> onWillPop(BuildContext context,String message) async {
    return MyPopup.showPopUp(
      context: context,
      title: AppLocalizations.of(context).getMessageByLangAndKey(
          appLanguageCode, 'confirmation'),
      message: AppLocalizations.of(context).getMessageByLangAndKey(
          appLanguageCode, message),
      function: () {
        SystemNavigator.pop();
      },
    )??
    false;
  }
}