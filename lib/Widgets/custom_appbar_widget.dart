import 'package:flutter/material.dart';
import 'package:lookinservice/PreDefined/app_constants.dart';
import 'package:lookinservice/Widgets/user_photo.dart';
import 'text_widgets.dart';

class CustomAppBar {
  static Widget appBar(BuildContext context, String title, Widget bottom) {
    return AppBar(
      elevation: 0,
      backgroundColor: primaryColorLight,
      title:
          MyText.mTextBold(context: context, text: title, color: Colors.black),
      centerTitle: true,
      // actions: <Widget>[
      //  NotificationIcon()],
      bottom: bottom,
    );
  }

  static Widget appBarNL(BuildContext context, String title) {
    return AppBar(
      elevation: 0,
      backgroundColor: primaryColorLight,
      title: MyText.mTextNLBold(
          context: context, text: title, color: Colors.black),
      centerTitle: true,
      // actions: <Widget>[
      //    NotificationIcon()
      // ],
    );
  }

  static Widget homeAndMoreAppBar(BuildContext context) {
    return Container(
      color: primaryColorLight,
      padding: EdgeInsets.symmetric(horizontal: hBlock * 5),
      height: 80,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
              height: 80,
              width: 80,
              child: UserPhoto(imageUrl: constLoginModel.image, height: 80)),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                children: <Widget>[
                  MyText.mTextBold(context: context, text: 'Welcome'),
                  Text(
                    ', ${constLoginModel.username}',
                    style:
                        TextStyle(fontSize: mFont, fontWeight: FontWeight.bold),
                  )
                ],
              ),
              SizedBox(
                height: 3,
              ),
              MyText.sText(
                  context: context, text: 'Welcome_Again', color: primaryColor)
            ],
          ),
          Spacer(),
          //  NotificationIcon()
        ],
      ),
    );
  }
}
