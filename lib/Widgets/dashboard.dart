import 'package:flutter/material.dart';
import 'package:lookinservice/Views/More/more_page.dart';
import 'package:lookinservice/Views/booking.dart';
import 'package:lookinservice/Views/home.dart';
import 'package:lookinservice/Views/Messages/messages_page.dart';
import 'package:lookinservice/Widgets/alert_widget.dart';
import '../PreDefined/app_constants.dart';
import '../Widgets/bottom_bar_widget.dart';

class Dashboard extends StatefulWidget {
  static const tag = '/dashBoard';

  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard>
    with SingleTickerProviderStateMixin {
  var _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        return MyPopup.onWillPop(context,'are_you_sure_close_app');
      },
      child: Directionality(
        textDirection: isRightToLeft ? TextDirection.rtl : TextDirection.ltr,
        child: SafeArea(
          top: false,
          bottom: false,
          child: Scaffold(
            key: _scaffoldKey,
            bottomNavigationBar: AppBottomBar(
              onBottomTabSelect: (int index) {
                onTabTapped(index);
              },
              currentbarIndex: currentbarIndex,
            ),
            body: getBody(),
          ),
        ),
      ),
    );
  }

  ///Get body of bottom tabs
  Widget getBody() {
    Widget body;
    switch (currentbarIndex) {
      case 0:
        body = HomePage();
        break;
      case 1:
        body = BookingsPage();
        break;
      case 2:
        body = MessagesPage();
        break;
      case 3:
        body = MorePage();
        break;
      // case 4:
      //   body = MorePage();
      //   break;
    }
    return body;
  }

  ///Called on tab of bottom navigation bar
  void onTabTapped(int index) {
    setState(() {
      currentbarIndex = index;
    });
  }
}
