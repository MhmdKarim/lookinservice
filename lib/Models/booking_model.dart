import 'package:lookinservice/Models/home_model.dart';
import 'package:lookinservice/Models/reasons_model.dart';

class BookingModel {
  int currentPage;
  List<BookingDataModel> data;
  String firstPageUrl;
  int from;
  int lastPage;
  String lastPageUrl;
  String nextPageUrl;
  String path;
  int perPage;
  String prevPageUrl;
  int to;
  int total;

  BookingModel(
      {this.currentPage,
      this.data,
      this.firstPageUrl,
      this.from,
      this.lastPage,
      this.lastPageUrl,
      this.nextPageUrl,
      this.path,
      this.perPage,
      this.prevPageUrl,
      this.to,
      this.total});

  BookingModel.fromJson(Map<String, dynamic> json) {
    currentPage = json['current_page'];
    if (json['data'] != null) {
      data = new List<BookingDataModel>();
      json['data'].forEach((v) {
        data.add(new BookingDataModel.fromJson(v));
      });
    }
    firstPageUrl = json['first_page_url'];
    from = json['from'];
    lastPage = json['last_page'];
    lastPageUrl = json['last_page_url'];
    nextPageUrl = json['next_page_url'];
    path = json['path'];
    perPage = json['per_page'];
    prevPageUrl = json['prev_page_url'];
    to = json['to'];
    total = json['total'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['current_page'] = this.currentPage;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    data['first_page_url'] = this.firstPageUrl;
    data['from'] = this.from;
    data['last_page'] = this.lastPage;
    data['last_page_url'] = this.lastPageUrl;
    data['next_page_url'] = this.nextPageUrl;
    data['path'] = this.path;
    data['per_page'] = this.perPage;
    data['prev_page_url'] = this.prevPageUrl;
    data['to'] = this.to;
    data['total'] = this.total;
    return data;
  }
}

class BookingDataModel {
  int id;
  String time;
  String date;
  int slotNo;
  int status;
  String note;
  UserModel user;
  ReasonsDataModel reason;

  BookingDataModel(
      {this.id,
      this.time,
      this.date,
      this.slotNo,
      this.status,
      this.note,
      this.user,
      this.reason});

  BookingDataModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    time = json['time'];
    date = json['date'];
    slotNo = json['slot_no'];
    status = json['status'];
    note = json['note'];
    user = json['user'] != null ? new UserModel.fromJson(json['user']) : null;
    reason =
        json['reason'] != null ? new ReasonsDataModel.fromJson(json['reason']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['time'] = this.time;
    data['date'] = this.date;
    data['slot_no'] = this.slotNo;
    data['status'] = this.status;
    data['note'] = this.note;
    if (this.user != null) {
      data['user'] = this.user.toJson();
    }
    if (this.reason != null) {
      data['reason'] = this.reason.toJson();
    }
    return data;
  }
}

