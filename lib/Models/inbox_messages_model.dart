// To parse this JSON data, do
//
//     final inboxMessagesModel = inboxMessagesModelFromJson(jsonString);

import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:lookinservice/Models/booking_model.dart';

InboxMessagesModel inboxMessagesModelFromJson(String str) => InboxMessagesModel.fromJson(json.decode(str));

String inboxMessagesModelToJson(InboxMessagesModel data) => json.encode(data.toJson());

class InboxMessagesModel {
    InboxMessagesModel({
        this.booking,
        this.inbox,
    });

    BookingDataModel booking;
    List<Inbox> inbox;

    factory InboxMessagesModel.fromJson(Map<String, dynamic> json) => InboxMessagesModel(
        booking: BookingDataModel.fromJson(json["booking"]),
        inbox: List<Inbox>.from(json["inbox"].map((x) => Inbox.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "booking": booking.toJson(),
        "inbox": List<dynamic>.from(inbox.map((x) => x.toJson())),
    };
}

// class Booking {
//     Booking({
//         this.id,
//         this.time,
//         this.date,
//         this.slotNo,
//         this.status,
//         this.note,
//     });

//     int id;
//     String time;
//     String date;
//     int slotNo;
//     int status;
//     String note;

//     factory Booking.fromJson(Map<String, dynamic> json) => Booking(
//         id: json["id"],
//         time: json["time"],
//         date: json["date"],
//         slotNo: json["slot_no"],
//         status: json["status"],
//         note: json["note"],
//     );

//     Map<String, dynamic> toJson() => {
//         "id": id,
//         "time": time,
//         "date": date,
//         "slot_no": slotNo,
//         "status": status,
//         "note": note,
//     };
// }

class Inbox {
    Inbox({
        this.id,
        this.msg,
        this.senderType,
        this.date,
        this.animationController
    });

    int id;
    String msg;
    int senderType;
    int date;
    AnimationController animationController;

    factory Inbox.fromJson(Map<String, dynamic> json) => Inbox(
        id: json["id"],
        msg: json["msg"],
        senderType: json["sender_type"],
        date: json["date"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "msg": msg,
        "sender_type": senderType,
        "date": date,
    };
}
