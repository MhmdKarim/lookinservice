class ApiResponseModel{
  int statuscode;
  String message;
  bool isError;
  dynamic data;
  
  ApiResponseModel.fromJson(obj) {
    this.statuscode = obj['statusCode'];
    this.message = obj['message'];
    this.isError = obj['isError'];
    this.data = obj['data'];
  }

}