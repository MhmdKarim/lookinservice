// class ReasonsModel {
//   int id;
//   String reason;

//   ReasonsModel({this.id, this.reason});

//   ReasonsModel.fromJson(Map<String, dynamic> json) {
//     id = json['id'];
//     reason = json['reason'];
//   }

//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['id'] = this.id;
//     data['reason'] = this.reason;
//     return data;
//   }
// }
class ReasonsModel {
  int statusCode;
  String message;
  bool isError;
  List<ReasonsDataModel> data;

  ReasonsModel({this.statusCode, this.message, this.isError, this.data});

  ReasonsModel.fromJson(Map<String, dynamic> json) {
    statusCode = json['statusCode'];
    message = json['message'];
    isError = json['isError'];
    if (json['data'] != null) {
      data = new List<ReasonsDataModel>();
      json['data'].forEach((v) {
        data.add(new ReasonsDataModel.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['statusCode'] = this.statusCode;
    data['message'] = this.message;
    data['isError'] = this.isError;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class ReasonsDataModel {
  int id;
  String reason;

  ReasonsDataModel({this.id, this.reason});

  ReasonsDataModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    reason = json['reason'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['reason'] = this.reason;
    return data;
  }
}
