class Area {
    Area({
        this.id,
        this.name,
        this.countryId,
        this.isSelected

    });

    int id;
    String name;
    int countryId;
    bool isSelected;

     Area.fromJson(Map<String, dynamic> json) {
        id = json["id"];
        name= json["name"];
        countryId= json["country_id"];
        isSelected =false;
     }

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "country_id": countryId,
    };
}
