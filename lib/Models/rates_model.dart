import 'package:lookinservice/Models/home_model.dart';

class RatesModel {
  int rate;
  int rateNo;
  List<RatesDataModel> rates;

  RatesModel({this.rate, this.rateNo, this.rates});

  RatesModel.fromJson(Map<String, dynamic> json) {
    rate = json['rate'];
    rateNo = json['rate_no'];
    if (json['rates'] != null) {
      rates = new List<RatesDataModel>();
      json['rates'].forEach((v) {
        rates.add(new RatesDataModel.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['rate'] = this.rate;
    data['rate_no'] = this.rateNo;
    if (this.rates != null) {
      data['rates'] = this.rates.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class RatesDataModel {
  int id;
  String comment;
  int rate;
  int date;
  UserModel user;

  RatesDataModel({this.id, this.comment, this.rate, this.date, this.user});

  RatesDataModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    comment = json['comment'];
    rate = json['rate'];
    date = json['date'];
    user = json['user'] != null ? new UserModel.fromJson(json['user']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['comment'] = this.comment;
    data['rate'] = this.rate;
    data['date'] = this.date;
    if (this.user != null) {
      data['user'] = this.user.toJson();
    }
    return data;
  }
}

