class LoginModel {
  int id;
  String username;
  String phone;
  String email;
  String commercialRecord;
  String taxCard;
  String addressAr;
  String addressEn;
  double latitude;
  double longitude;
  int rate;
  int rateNo;
  int available;
  String rememberToken;
  String image;
  MainStore mainStore;

  LoginModel(
      {this.id,
      this.username,
      this.phone,
      this.email,
      this.commercialRecord,
      this.taxCard,
      this.addressAr,
      this.addressEn,
      this.latitude,
      this.longitude,
      this.rate,
      this.rateNo,
      this.available,
      this.rememberToken,
      this.image,
      this.mainStore});

  LoginModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    username = json['username'];
    phone = json['phone'];
    email = json['email'];
    commercialRecord = json['commercial_record'];
    taxCard = json['tax_card'];
    addressAr = json['address_ar'];
    addressEn = json['address_en'];
    latitude = json['latitude'];
    longitude = json['longitude'];
    rate = json['rate'];
    rateNo = json['rate_no'];
    available = json['available'];
    rememberToken = json['remember_token'];
    image = json['image'];
    mainStore = json['main_store'] != null
        ? new MainStore.fromJson(json['main_store'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['username'] = this.username;
    data['phone'] = this.phone;
    data['email'] = this.email;
    data['commercial_record'] = this.commercialRecord;
    data['tax_card'] = this.taxCard;
    data['address_ar'] = this.addressAr;
    data['address_en'] = this.addressEn;
    data['latitude'] = this.latitude;
    data['longitude'] = this.longitude;
    data['rate'] = this.rate;
    data['rate_no'] = this.rateNo;
    data['available'] = this.available;
    data['remember_token'] = this.rememberToken;
    data['image'] = this.image;
    if (this.mainStore != null) {
      data['main_store'] = this.mainStore.toJson();
    }
    return data;
  }
}

class MainStore {
  int id;
  String nameAr;
  String nameEn;
  String phone;
  String email;
  String image;
  String commercialRecord;
  String taxCard;
  String descAr;
  String descEn;
  String termsAr;
  String termsEn;
  String code;

  MainStore(
      {this.id,
      this.nameAr,
      this.nameEn,
      this.phone,
      this.email,
      this.image,
      this.commercialRecord,
      this.taxCard,
      this.descAr,
      this.descEn,
      this.termsAr,
      this.termsEn,
      this.code});

  MainStore.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    nameAr = json['name_ar'];
    nameEn = json['name_en'];
    phone = json['phone'];
    email = json['email'];
    image = json['image'];
    commercialRecord = json['commercial_record'];
    taxCard = json['tax_card'];
    descAr = json['desc_ar'];
    descEn = json['desc_en'];
    termsAr = json['terms_ar'];
    termsEn = json['terms_en'];
    code = json['code'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name_ar'] = this.nameAr;
    data['name_en'] = this.nameEn;
    data['phone'] = this.phone;
    data['email'] = this.email;
    data['image'] = this.image;
    data['commercial_record'] = this.commercialRecord;
    data['tax_card'] = this.taxCard;
    data['desc_ar'] = this.descAr;
    data['desc_en'] = this.descEn;
    data['terms_ar'] = this.termsAr;
    data['terms_en'] = this.termsEn;
    data['code'] = this.code;
    return data;
  }
}
