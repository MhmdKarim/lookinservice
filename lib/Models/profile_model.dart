class ProfileDataModel {
  int id;
  String username;
  String phone;
  String email;
  String commercialRecord;
  String taxCard;
  String addressAr;
  String addressEn;
  double latitude;
  double longitude;
  double rate;
  int rateNo;
  int available;
  String rememberToken;

  ProfileDataModel(
      {this.id,
      this.username,
      this.phone,
      this.email,
      this.commercialRecord,
      this.taxCard,
      this.addressAr,
      this.addressEn,
      this.latitude,
      this.longitude,
      this.rate,
      this.rateNo,
      this.available,
      this.rememberToken});

  ProfileDataModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    username = json['username'];
    phone = json['phone'];
    email = json['email'];
    commercialRecord = json['commercial_record'];
    taxCard = json['tax_card'];
    addressAr = json['address_ar'];
    addressEn = json['address_en'];
    latitude = json['latitude'];
    longitude = json['longitude'];
    rate = json['rate'];
    rateNo = json['rate_no'];
    available = json['available'];
    rememberToken = json['remember_token'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['username'] = this.username;
    data['phone'] = this.phone;
    data['email'] = this.email;
    data['commercial_record'] = this.commercialRecord;
    data['tax_card'] = this.taxCard;
    data['address_ar'] = this.addressAr;
    data['address_en'] = this.addressEn;
    data['latitude'] = this.latitude;
    data['longitude'] = this.longitude;
    data['rate'] = this.rate;
    data['rate_no'] = this.rateNo;
    data['available'] = this.available;
    data['remember_token'] = this.rememberToken;
    return data;
  }
}
