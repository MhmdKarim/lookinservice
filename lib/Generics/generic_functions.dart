class GenericFunctions{

 static   intToDate(int timeInMillis) {
    var datetime = DateTime.fromMillisecondsSinceEpoch(timeInMillis);
    String stringifiedDatetime = datetime.toString();
    var selectedDate = stringifiedDatetime.substring(0, 10);
    return selectedDate;
  }

 static intToTime(int timeInMillis) {
    var datetime = DateTime.fromMillisecondsSinceEpoch(timeInMillis);
    String stringifiedDatetime = datetime.toString();
    var selectedTime = stringifiedDatetime.substring(11, 16);
    return selectedTime;
  }
}