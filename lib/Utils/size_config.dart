import 'package:flutter/widgets.dart';

class SizeConfig {
  static MediaQueryData mediaQueryData;
  static double screenWidth;
  static double screenHeight;
  static double hBlock;
  static double vBlock;

  static double xlFont = hBlock * 7.0;
  static double lFontSize = hBlock * 6.0;
  static double mFont = hBlock * 5.0;
  static double sFont = hBlock * 4.0;
  static double xsFont = hBlock * 3.0;

 static void init(BuildContext context) {
    mediaQueryData = MediaQuery.of(context);
    screenWidth = mediaQueryData.size.width;
    screenHeight = mediaQueryData.size.height;
    hBlock = screenWidth > 450? screenWidth / 120 : screenWidth / 100;
    vBlock = screenWidth > 450? screenHeight / 150 : screenHeight / 100;
  }
}
