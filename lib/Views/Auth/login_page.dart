import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:lookinservice/Generics/NetworkUtil.dart';
import 'package:lookinservice/Generics/app_prefs.dart';
import 'package:lookinservice/Models/api_response_model.dart';
import 'package:lookinservice/Models/login_model.dart';
import 'package:lookinservice/PreDefined/app_constants.dart';
import 'package:lookinservice/PreDefined/localization.dart';
import 'package:lookinservice/Utils/size_config.dart';
import 'package:lookinservice/Views/Auth/branch_registration.dart';
import 'package:lookinservice/Views/Auth/restore_password.dart';
import 'package:lookinservice/Views/Auth/trade_mark_registration.dart';
import 'package:lookinservice/Widgets/alert_widget.dart';
import 'package:lookinservice/Widgets/button.dart';
import 'package:lookinservice/Widgets/dashboard.dart';
import 'package:lookinservice/Widgets/text_widgets.dart';

class LoginPage extends StatefulWidget {
  static const tag = '/LoginPage';

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  AppLocalizations appLocalizations;
  final _userController = TextEditingController();
  final _passwordController = TextEditingController();
  final RoundedLoadingButtonController _btnController =
      new RoundedLoadingButtonController();
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final _formKey = GlobalKey<FormState>();

  @override
  void dispose() {
    _userController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig.init(context);
    appLocalizations = AppLocalizations.of(context);
    return WillPopScope(
      onWillPop: () {
        return MyPopup.onWillPop(context, 'are_you_sure_close_app');
      },
      child: Directionality(
        textDirection: isRightToLeft ? TextDirection.rtl : TextDirection.ltr,
        child: Scaffold(
          key: _scaffoldKey,
          body: SingleChildScrollView(
            child: Form(
              key: _formKey,
              child: Column(
                //mainAxisAlignment: MainAxisAlignment.spaceAround,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.all(50),
                    height: vBlock * 12,
                    child: Center(child: Image.asset('assets/icon-App.png')),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 50.0),
                    child: MyText.mTextBold(
                      context: context,
                      text: 'Login',
                      // color: primaryColor
                    ),
                  ),
                  _buildTextField(
                      context: context,
                      title: 'User_name',
                      controller: _userController,
                      obsecure: false),
                  _buildTextField(
                      context: context,
                      title: 'Password',
                      controller: _passwordController,
                      obsecure: true),
                  Row(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 8),
                        child: InkWell(
                          onTap: () {
                            Navigator.of(context)
                                .pushNamed(RestorePasswordPage.tag);
                          },
                          child: MyText.mTextBold(
                              context: context,
                              text: 'Restore_your_password',
                              color: primaryColor),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 20),
                  Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: RoundedLoadingButton(
                      color: primaryColor,
                      width: hBlock * 85,
                      controller: _btnController,
                      onPressed: () {
                        if (_formKey.currentState.validate()) {
                          postLogin();
                        }
                      },
                      child: MyText.mText(
                          context: context, text: 'Login', color: Colors.white),
                    ),
                  ),
                  SizedBox(height: 30),
                  _borderedBtn(
                      context: context,
                      title: 'Trade_mark_registration',
                      onPressed: () {
                        _btnController.reset();
                        Navigator.of(context)
                            .pushNamed(TradeMarkRegistration.tag);
                      }),
                  _borderedBtn(
                      context: context,
                      title: 'Trade_mark_branch_registration',
                      onPressed: () {
                        _btnController.reset();
                        Navigator.of(context)
                            .pushNamed(BranchRegistrationPage.tag);
                      }),
                  Container(),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildTextField(
      {BuildContext context,
      String title,
      TextEditingController controller,
      bool obsecure}) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          MyText.sTextBold(context: context, text: title),
          TextFormField(
            validator: (value) {
              if (value.isEmpty) {
                _btnController.reset();
                return appLocalizations.getMessageByLangAndKey(
                        appLanguageCode, 'Enter') +
                    ' ' +
                    appLocalizations.getMessageByLangAndKey(
                        appLanguageCode, title);
              } else {
                return null;
              }
            },
            obscureText: obsecure,
            decoration: InputDecoration(
              hintStyle: TextStyle(color: accentColor),
              hintText: appLocalizations.getMessageByLangAndKey(
                  appLanguageCode, title),
            ),
            controller: controller,
            cursorColor: primaryColor,
          ),
          SizedBox(
            height: 20,
          ),
        ],
      ),
    );
  }

  Widget _borderedBtn(
      {BuildContext context, String title, Function onPressed}) {
    return Container(
      margin: EdgeInsets.all(20),
      width: hBlock * 85,
      height: 50,
      decoration: BoxDecoration(
        color: canvusColor,
        border: Border.all(width: 1.0, color: primaryColor),
        borderRadius: BorderRadius.all(
            Radius.circular(10.0) //         <--- border radius here
            ),
      ),
      child: RaisedButton(
          //color: primaryColor,
          onPressed: onPressed,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10),
          ),
          child: MyText.mText(
            context: context,
            text: title,
            color: primaryColor,
          )),
    );
  }

  postLogin() async {
    Map<String, dynamic> jsonMap = {
      "username": _userController.text,
      "password": _passwordController.text,
    };
    String jsonString = json.encode(jsonMap);
    ApiResponseModel responseModel = await NetworkUtil.postWithoutToken(
      'login',
      jsonString,
    );
    if (responseModel == null) {
      return;
    }
    if (responseModel.statuscode == 200) {
      LoginModel model = LoginModel.fromJson(responseModel.data);
      if (model == null) {
        return;
      }
      String loginModel = json.encode(model);
      print(loginModel);
      savePrefs('loginModel', loginModel);
      savePrefsbool('isLoggedIn', true);
      constLoginModel = model;
      currentbarIndex = 0;
      //isLoggedIn = true;
      _btnController.success();
      //widget.onLogin();
      Navigator.of(context).pushReplacementNamed(Dashboard.tag);
    } else {
      _btnController.reset();
      _scaffoldKey.currentState.showSnackBar(
        SnackBar(
          backgroundColor: Colors.red,
          content: Text(
            responseModel.message,
            textAlign: TextAlign.center,
          ),
        ),
      );
    }
  }
}
