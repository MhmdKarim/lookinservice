import 'dart:convert';
import 'dart:io';

import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:lookinservice/Generics/NetworkUtil.dart';
import 'package:lookinservice/Models/api_response_model.dart';
import 'package:lookinservice/Models/brand_filter_model.dart';
import 'package:lookinservice/PreDefined/app_constants.dart';
import 'package:lookinservice/PreDefined/localization.dart';
import 'package:lookinservice/Utils/size_config.dart';
import 'package:lookinservice/Views/Auth/login_page.dart';
import 'package:lookinservice/Views/success_page.dart';
import 'package:lookinservice/Widgets/button.dart';
import 'package:lookinservice/Widgets/text_widgets.dart';

class TradeMarkRegistration extends StatefulWidget {
  static const tag = '/TradeMarkRegistration';

  @override
  _TradeMarkRegistrationState createState() => _TradeMarkRegistrationState();
}

class _TradeMarkRegistrationState extends State<TradeMarkRegistration> {
  AppLocalizations appLocalizations;
  String _logoBase64;
  File _logoImage;
  String _taxBase64;
  File _taxImage;
  String _commercialBase64;
  File _commercialImage;
  final _formKey = GlobalKey<FormState>();
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final _nameArabicController = TextEditingController();
  final _nameEnglishController = TextEditingController();
  final _briefArabicController = TextEditingController();
  final _briefEnglishController = TextEditingController();
  final _termsArabicController = TextEditingController();
  final _termsEnglishController = TextEditingController();
  final _phoneController = TextEditingController();
  final _emailController = TextEditingController();
  final RoundedLoadingButtonController _btnController =
      new RoundedLoadingButtonController();
  // File _galleryFile;
  List<File> _chosenPhotos = [];
  List<String> _chosenPhotosBase64 = [];
  List<Categories> _categoryList = [];
  List<SubCategories> _subCategoryList = [];
  Categories _selectedCategory = Categories();
  SubCategories _selectedSubCategory = SubCategories();
  final _picker = ImagePicker();
  @override
  void initState() {
    _selectedCategory.name = '';
    _selectedSubCategory.name = '';
    _getCatSubCat();
    super.initState();
  }

  @override
  void dispose() {
_nameArabicController.dispose();
_nameEnglishController.dispose();
_briefArabicController.dispose();
_briefEnglishController.dispose();
_termsArabicController.dispose();
_termsEnglishController.dispose();
_phoneController.dispose();
_emailController.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig.init(context);
    appLocalizations = AppLocalizations.of(context);
    return Directionality(
      textDirection: isRightToLeft ? TextDirection.rtl : TextDirection.ltr,
      child: Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
            centerTitle: true,
            elevation: 0,
            backgroundColor: primaryColorLight,
            title: MyText.mTextBold(
              context: context,
              text: 'Trade_mark_registration',
            )),
        body: SingleChildScrollView(
          child: Form(
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: MyText.sText(
                      context: context,
                      text: 'Now_You_are_registerating_your_trade_mark__'),
                ),
                Center(child: _logoWidget()),
                Center(child: MyText.sText(context: context, text: 'Logo')),
                Padding(
                  padding: const EdgeInsets.all(10),
                  child: MyText.sText(
                      context: context, text: 'Please_attach_logo'),
                ),
                _buildSelectedImages(),
                _buildTextField(
                    context: context,
                    title: 'Trade_mark_in_Arabic',
                    hint: 'Trade_mark_in_Arabic',
                    controller: _nameArabicController),
                _buildTextField(
                    context: context,
                    title: 'Trade_mark_in_English',
                    hint: 'Trade_mark_in_English',
                    controller: _nameEnglishController),
                _buildTextField(
                    context: context,
                    title: 'Short_brief_about_trade_mark_Arabic',
                    hint: 'Short_brief_about_trade_mark_Arabic',
                    controller: _briefArabicController,
                    maxLines: null),
                _buildTextField(
                    context: context,
                    title: 'Short_brief_about_trade_mark_English',
                    hint: 'Short_brief_about_trade_mark_English',
                    controller: _briefEnglishController,
                    maxLines: null),
                _buildListTile(
                  context: context,
                  title: 'Category',
                  image: 'categories',
                  selected: _selectedCategory.name,
                  function: () {
                    setState(() {
                      // _categoryChips.clear();
                    });
                    _showSheet(
                      context,
                      _sheet(
                        title: 'Category',
                        length: _categoryList.length,
                        function: (context, index) {
                          return _buildCategoryItem(
                              context: context, index: index);
                        },
                      ),
                    );
                  },
                ),
                _buildListTile(
                  context: context,
                  title: 'Subcategory',
                  image: 'categories',
                  selected: _selectedSubCategory.name,
                  function: () {
                    setState(() {
                      // _categoryChips.clear();
                      //_categoryChipsId.clear();
                    });
                    _showSheet(
                      context,
                      _sheet(
                        title: 'Subcategory',
                        length: _categoryList.length,
                        function: (context, index) {
                          return _buildSubCategoryItem(
                              context: context, index: index);
                        },
                      ),
                    );
                  },
                ),
                _buildTextField(
                    context: context,
                    title: 'Phone_Number',
                    hint: 'Phone_Number',
                    controller: _phoneController),
                _buildTextField(
                    context: context,
                    title: 'email',
                    hint: 'email',
                    controller: _emailController),
                Container(
                  height: vBlock * 30,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      _documentItem(
                          title: 'tax_card',
                          function: () {
                            _pickImageTx(ImageSource.gallery);
                          },
                          file: _taxImage),
                      _documentItem(
                          title: 'commercial_record',
                          function: () {
                            _pickImageCom(ImageSource.gallery);
                          },
                          file: _commercialImage),
                      // _documentItemCom(title: 'commercial_record'),
                    ],
                  ),
                ),
                _buildTextField(
                    context: context,
                    title: 'Terms_conditions_in_Arabic',
                    hint: 'Terms_conditions_in_Arabic',
                    controller: _termsArabicController,
                    maxLines: null),
                _buildTextField(
                    context: context,
                    title: 'Terms_conditions_in_English',
                    hint: 'Terms_conditions_in_English',
                    controller: _termsEnglishController,
                    maxLines: null),
                Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: RoundedLoadingButton(
                    color: primaryColor,
                    width: hBlock * 85,
                    controller: _btnController,
                    onPressed: () {
                      if (_formKey.currentState.validate() && _validation()) {
                        _postTradeMarkReg();
                      } else {
                        _btnController.reset();
                        _scaffoldKey.currentState.showSnackBar(
                          SnackBar(
                            backgroundColor: Colors.red,
                            content: MyText.sText(
                              context: context,
                              text: 'Please_fill_all',
                            ),
                          ),
                        );
                      }
                    },
                    child: MyText.mText(
                        context: context,
                        text: "Trade_mark_registration",
                        color: Colors.white),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  bool _validation() {
    if (_chosenPhotos.length == 0 ||
        _taxImage == null ||
        _logoImage == null ||
        _commercialImage == null ||
        _selectedCategory == null ||
        _selectedSubCategory == null) {
      return false;
    } else {
      return true;
    }
  }

  Widget _buildSelectedImages() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: DottedBorder(
        color: accentColor,
        borderType: BorderType.RRect,
        radius: Radius.circular(12),
        padding: EdgeInsets.all(6),
        child: ClipRRect(
          borderRadius: BorderRadius.all(Radius.circular(12)),
          child: Container(
            height: 150,
            child: Row(
              children: <Widget>[
                Container(
                    width: 50,
                    child: IconButton(
                        icon: Icon(Icons.add),
                        onPressed: () {
                          _pickImages(ImageSource.gallery);
                        })),
                Visibility(
                  visible: _chosenPhotos.length == 0 ? false :  true,
                  child: Expanded(
                    child: ListView.builder(
                        scrollDirection: Axis.horizontal,
                        itemCount: _chosenPhotos.length,
                        itemBuilder: (context, index) {
                          //  new Padding(
                          //       padding: const EdgeInsets.all(5.0),
                          //       child: new Image.file(
                          //         new File(images[index].toString()),
                          //       ),
                          //     );
                          //       itemCount: images.length;
                          return displaySelectedFiles(
                              _chosenPhotos[index], index);
                        }),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void _showSheet(BuildContext ctx, Widget sheet) {
    showModalBottomSheet<dynamic>(
      isScrollControlled: true,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20.0), topRight: Radius.circular(20.0)),
      ),
      context: ctx,
      builder: (context) {
        return Wrap(
          children: <Widget>[sheet],
        );
      },
    );
  }

  Widget _buildListTile(
      {BuildContext context,
      String title,
      String image,
      Function function,
      String selected}) {
    return Column(
      children: <Widget>[
        ListTile(
            title: selected == ''
                ? MyText.sText(
                    context: context,
                    text: title,
                  )
                : MyText.sTextNL(
                    context: context,
                    text: selected,
                  ),
            trailing: Icon(
              Icons.arrow_forward_ios,
              size: 20,
            ),
            onTap: function),
        Container(
          height: 1,
          width: hBlock * 95,
          color: accentColor,
        )
      ],
    );
  }

  Widget _buildCategoryItem({BuildContext context, int index}) {
    return Column(
      children: <Widget>[
        ListTile(
          title: Text(_categoryList[index].name),
          trailing: Icon(Icons.arrow_forward_ios),
          onTap: () {
            Navigator.of(context).pop();
            print(_categoryList[index].id);
            setState(() {
              _selectedCategory = _categoryList[index];
            });
          },
        ),
        Container(height: 1, width: double.infinity, color: primaryColor)
      ],
    );
  }

  Widget _buildSubCategoryItem({BuildContext context, int index}) {
    return Column(
      children: <Widget>[
        ListTile(
            title: Text(_subCategoryList[index].name),
            trailing: Icon(Icons.arrow_forward_ios),
            onTap: () {
              Navigator.of(context).pop();
              print(_subCategoryList[index].name);
              setState(() {
                _selectedSubCategory = _subCategoryList[index];
              });
            }),
        Container(
          height: 1,
          width: double.infinity,
          color: accentColor,
        )
      ],
    );
  }

  Widget _sheet({String title, int length, Function function}) {
    return Directionality(
      textDirection: isRightToLeft ? TextDirection.rtl : TextDirection.ltr,
      child: StatefulBuilder(
          builder: (BuildContext context, StateSetter setState) {
        return SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.only(
              top: 10,
              left: 10,
              right: 10,
              bottom: MediaQuery.of(context).viewInsets.bottom + 10,
            ),
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Center(
                      child: Container(
                        height: 5,
                        width: 50,
                        color: accentColor,
                      ),
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      SizedBox(
                        width: 46,
                      ),
                      MyText.lText(context: context, text: title),
                      IconButton(
                        icon: Icon(Icons.close),
                        iconSize: 30,
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                      ),
                    ],
                  ),
                  ListView.builder(
                    scrollDirection: Axis.vertical,
                    physics: ScrollPhysics(),
                    shrinkWrap: true,
                    itemCount: length,
                    itemBuilder: function,
                  ),
                  // Btn.button(context: context, title: 'Done', onPressed: () {})
                ],
              ),
            ),
          ),
        );
      }),
    );
  }

  Widget _logoWidget() {
    return Container(
      padding: const EdgeInsets.only(top: 10, bottom: 5),
      child: Center(
        child: Stack(
          overflow: Overflow.visible,
          fit: StackFit.passthrough,
          children: <Widget>[
            ClipRRect(
                child: _logoImage == null
                    ? CircleAvatar(
                        radius: (hBlock * 25 / 2) + 2,
                        backgroundColor: accentColor,
                        child: CircleAvatar(
                          backgroundColor: accentColor,
                          radius: hBlock * 8,
                          child: Image.asset(
                            'assets/add-image.png',
                            fit: BoxFit.contain,
                            // width: height,
                          ),
                        ),
                      )
                    : Container(
                        decoration: BoxDecoration(
                            color: accentColor, shape: BoxShape.circle),
                        child: CircleAvatar(
                          backgroundImage: FileImage(_logoImage),
                          backgroundColor: Colors.transparent,
                          maxRadius: 48,
                        ),
                        height: hBlock * 25,
                        width: hBlock * 25,
                      )),
            Positioned(
              top: 0,
              bottom: 0,
              right: -hBlock * 5,
              child: Container(
                height: hBlock * 10,
                width: hBlock * 10,
                decoration: BoxDecoration(
                    border: Border.all(width: 1, color: primaryColor),
                    color: primaryColorLight,
                    shape: BoxShape.circle),
                child: IconButton(
                  onPressed: () {
                    _pickImageLogo(ImageSource.gallery);
                  },
                  icon: Icon(
                    Icons.edit,
                    size: hBlock * 5,
                    color: primaryColor,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  _pickImageLogo(ImageSource _imageSource) async {
    PickedFile pickedFile = await _picker.getImage(source: _imageSource);
    final File _file = File(pickedFile.path);
    List<int> imageBytes = _file.readAsBytesSync();
    _logoBase64 = base64Encode(imageBytes);
    if (_file != null) {
      setState(() {
        _logoImage = _file;
      });
    }
  }

  _pickImageTx(ImageSource _imageSource) async {
    PickedFile pickedFile = await _picker.getImage(source: _imageSource);
    final File _file = File(pickedFile.path);
    List<int> imageBytes = _file.readAsBytesSync();
    _taxBase64 = base64Encode(imageBytes);
    if (_file != null) {
      setState(() {
        _taxImage = _file;
        //print(file.path);
      });
    }
  }

  _pickImageCom(ImageSource _imageSource) async {
    PickedFile pickedFile = await _picker.getImage(source: _imageSource);
    final File _file = File(pickedFile.path);
    List<int> imageBytes = _file.readAsBytesSync();
    _commercialBase64 = base64Encode(imageBytes);
    if (_file != null) {
      setState(() {
        _commercialImage = _file;
        //print(file.path);
      });
    }
  }

  _pickImages(ImageSource _imageSource) async {
    PickedFile pickedFile = await _picker.getImage(source: _imageSource);
    final File _file = File(pickedFile.path);
    List<int> imageBytes = _file.readAsBytesSync();
    String _base64 = base64Encode(imageBytes);
    print('_chosenPhotosBase64.length:  ' +
        _chosenPhotosBase64.length.toString());
    if (_file != null) {
      setState(() {
        _chosenPhotos.add(_file);
        _chosenPhotosBase64.add(_base64);
      });
    }
    // setState(() {
    //   _galleryFile.path == null
    //       ? print("No Photo Chosen")
    //       : _chosenPhotos.add(_galleryFile);
    //   print("You selected gallery image : " + _galleryFile.path);
    // });
  }

  Widget _buildTextField(
      {BuildContext context,
      String title,
      String hint,
      TextEditingController controller,
      int maxLines = 1}) {
    return Padding(
      padding: const EdgeInsets.all(8),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          MyText.sText(context: context, text: title),
          TextFormField(
              maxLines: maxLines,
              decoration: InputDecoration(
                hintStyle: TextStyle(color: accentColor),
                hintText: appLocalizations.getMessageByLangAndKey(
                    appLanguageCode, hint),
              ),
              controller: controller,
              cursorColor: primaryColor,
              validator: (value) {
                if (value.isEmpty) {
                  _btnController.reset();
                  return appLocalizations.getMessageByLangAndKey(
                          appLanguageCode, 'Enter') +
                      ' ' +
                      appLocalizations.getMessageByLangAndKey(
                          appLanguageCode, title);
                } else {
                  return null;
                }
              }),
          SizedBox(
            height: 20,
          ),
        ],
      ),
    );
  }

  Widget _documentItem({String title, Function function, File file}) {
    return Padding(
      padding: EdgeInsets.all(hBlock * 2),
      child: DottedBorder(
        color: file == null? accentColor: primaryColor,
        borderType: BorderType.RRect,
        radius: Radius.circular(12),
        padding: EdgeInsets.all(hBlock * 2),
        child: ClipRRect(
          borderRadius: BorderRadius.all(Radius.circular(12)),
          child: Container(
            width: hBlock * 40,
            height: vBlock * 50,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                InkWell(
                  onTap: function,
                  child: file == null
                      ? Container(
                          height: hBlock * 25,
                          child: Image.asset('assets/add-image.png',
                              fit: BoxFit.fitHeight),
                        )
                      : Container(
                          decoration: BoxDecoration(
                              color: accentColor,
                              image: DecorationImage(
                                  image: FileImage(file), fit: BoxFit.cover)),
                          height: vBlock * 20,
                          width: hBlock * 30,
                        ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(vertical: hBlock * 2),
                  child: MyText.sText(context: context, text: title),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget displaySelectedFiles(File file, int index) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: DottedBorder(
        color: primaryColor,
        borderType: BorderType.RRect,
        radius: Radius.circular(12),
        padding: EdgeInsets.all(6),
        child: ClipRRect(
          borderRadius: BorderRadius.all(Radius.circular(12)),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              InkWell(
                child: Icon(Icons.delete, color: Colors.red, size: 25),
                onTap: () {
                  setState(() {
                    _chosenPhotos.remove(file);
                    _chosenPhotosBase64.removeAt(index);
                    print("_chosenPhotos.length " +
                        _chosenPhotos.length.toString());
                    print("_chosenPhotosBase64.length " +
                        _chosenPhotosBase64.length.toString());
                  });
                },
              ),
              Container(
                margin: const EdgeInsets.all(8.0),
                width: 80.0,
                height: 80.0,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5.0),
                  image: DecorationImage(
                      image: FileImage(file), fit: BoxFit.cover),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  _getCatSubCat() async {
    ApiResponseModel responseModel = await NetworkUtil.getWithoutToken(
      'brandFilter',
    );
    if (responseModel == null) {
      return;
    }
    if (responseModel.statuscode == 200) {
     
      BrandFilterModel model = BrandFilterModel.fromJson(responseModel.data);
      if (model == null) {
        return;
      }
      _categoryList = model.categories;
      _subCategoryList = model.subCategories;
      setState(() {
        // _isLoading = false;
      });
    } else {
      //_isLoading = false;
    }
  }

  _postTradeMarkReg() async {
    Map<String, dynamic> jsonMap = {
      "slider": _chosenPhotosBase64,
      "name_ar": _nameArabicController.text,
      "name_en": _nameEnglishController.text,
      "desc_ar": _briefArabicController.text,
      "desc_en": _briefEnglishController.text,
      "terms_ar": _termsArabicController.text,
      "terms_en": _termsEnglishController.text,
      "phone": _phoneController.text,
      "email": _emailController.text,
      "category_id": _selectedCategory.id.toString(),
      "sub_category_id": _selectedSubCategory.id.toString(),
      "image": _logoBase64,
      "tax_card": _taxBase64,
      "commercial_record": _commercialBase64,
    };
    String jsonString = json.encode(jsonMap);
    print(jsonString);
    ApiResponseModel responseModel = await NetworkUtil.postWithoutToken(
      'brandRegister',
      jsonString,
    );
    if (responseModel == null) {
      return;
    }
    print(responseModel.message);
    if (responseModel.statuscode == 200) {
      print(responseModel.message);
       print(responseModel.data);
      _btnController.success();
      Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => SuccessPage(
              appBarTitle: 'Registration_confirmation',
              title: 'Your_request_has_been_sent',
              text: 'Brand_request_sent_to_admin___',
              btnText: 'Back_to_registration',
              onPressed: () {
                Navigator.of(context).pushReplacementNamed(LoginPage.tag);
              },
              visible: true,
            ),
          ));
    } else {
      _btnController.reset();
    }
  }
}
