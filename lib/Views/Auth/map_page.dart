import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:lookinservice/PreDefined/app_constants.dart';
import 'package:lookinservice/PreDefined/localization.dart';
import 'package:lookinservice/Utils/size_config.dart';
import 'package:lookinservice/Widgets/text_widgets.dart';

class MapPage extends StatefulWidget {
  static const tag = '/MapPage';
  // final List<Marker> markers;
  final LatLng center;

  const MapPage({this.center});

  @override
  _MapPageState createState() => _MapPageState();
}

class _MapPageState extends State<MapPage> {
  AppLocalizations appLocalizations;
  GoogleMapController _mapController;
  List<Marker> _markers = <Marker>[];
  LatLng _center;

  void _onMapCreated(GoogleMapController controller) {
    _mapController = controller;
  }

  _handleTap(LatLng point) {
    print('Map tapped: yes');
    setState(() {
      if (_markers.length > 0) {
        _markers.removeLast();
      }

      _markers.add(Marker(
        markerId: MarkerId(point.toString()),
        position: point,
        infoWindow: InfoWindow(
          title: 'I am a marker',
        ),
        icon:
            BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueMagenta),
      ));
    });
    _center = LatLng(point.latitude, point.longitude);
    // print('${point.latitude}, ${point.longitude}');
     _mapController.moveCamera(CameraUpdate.newLatLng(_center));
  }

  _markerDraw() {
    _markers.add(
      Marker(
          markerId: MarkerId('SomeId'),
          position: widget.center,
          // infoWindow: InfoWindow(title: _profileDataModel.username),
          onTap: () {}),
    );
  }
  Future<bool> _willPopCallback() async {
    // await showDialog or Show add banners or whatever
    // then
    return false; // return true if the route to be popped
}

  @override
  void initState() {
    _markerDraw();
    _center = LatLng(widget.center.latitude, widget.center.longitude);
    print(_center.latitude);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig.init(context);
    appLocalizations = AppLocalizations.of(context);
    return WillPopScope(
      onWillPop: _willPopCallback,
          child: Directionality(
        textDirection: isRightToLeft ? TextDirection.rtl : TextDirection.ltr,
        child: Scaffold(
          appBar: PreferredSize(
            preferredSize: Size(double.infinity, kToolbarHeight),
            child: AppBar(
              elevation: 0,
              backgroundColor: primaryColorLight,
              title: MyText.mTextBold(
                  context: context,
                  text: 'Attach_your_location',
                  color: Colors.black),
              leading: new IconButton(
                icon: new Icon(Icons.close),
                onPressed: () => Navigator.pop(context, _center),
              ),
              centerTitle: true,
              actions: <Widget>[
                IconButton(
                  icon: Icon(Icons.refresh),
                  color: primaryColor,
                  iconSize: 35,
                  onPressed: () {
                    setState(() {
                      _markers = [];
                    });
                  },
                )
              ],
            ),
          ),
          body: Column(
            children: <Widget>[
              Expanded(
                child: GoogleMap(
                  markers: Set<Marker>.of(_markers),
                  onMapCreated: _onMapCreated,
                  initialCameraPosition: CameraPosition(
                    target: widget.center,
                    zoom: 16.0,
                  ),
                  onTap: _handleTap,
                ),
              ),
              Container(
                child: ListTile(
                    leading: Icon(
                      Icons.location_on,
                      color: primaryColor,
                      size: 20,
                    ),
                    title: MyText.sText(
                        context: context,
                        text: 'Attach_your_location',
                        color: primaryColor),
                    onTap: () {
                      Navigator.pop(context, _center);
                    }),
              ),
              SizedBox(height: 20)
            ],
          ),
        ),
      ),
    );
  }
}
