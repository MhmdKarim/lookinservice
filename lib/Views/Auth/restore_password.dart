import 'package:flutter/material.dart';
import 'package:lookinservice/PreDefined/app_constants.dart';
import 'package:lookinservice/PreDefined/localization.dart';
import 'package:lookinservice/Utils/size_config.dart';
import 'package:lookinservice/Widgets/standard_button_widget.dart';
import 'package:lookinservice/Widgets/text_widgets.dart';

class RestorePasswordPage extends StatefulWidget {
  static const tag = '/RestorePasswordPage';

  @override
  _RestorePasswordPageState createState() => _RestorePasswordPageState();
}

class _RestorePasswordPageState extends State<RestorePasswordPage> {
  AppLocalizations appLocalizations;
  final _emailController = TextEditingController();

  @override
  void dispose() {
    _emailController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig.init(context);
    appLocalizations = AppLocalizations.of(context);
    return Directionality(
      textDirection: isRightToLeft ? TextDirection.rtl : TextDirection.ltr,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: primaryColorLight,
          elevation: 0,
        ),
        body: SingleChildScrollView(
          child: Column(
            //mainAxisAlignment: MainAxisAlignment.spaceAround,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(bottom: 50),
                height: vBlock * 12,
                child: Center(child: Image.asset('assets/icon-App.png')),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 10.0),
                child: MyText.mTextBold(
                  context: context,
                  text: 'Restore_your_password',
                  // color: primaryColor
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 50.0),
                child: MyText.mText(
                  context: context,
                  text: 'Please_write_your_registered_email',
                ),
              ),
              _buildTextField(
                context: context,
                title: 'email',
                controller: _emailController,
              ),
              Btn.button(
                  context: context,
                  title: 'Restore_your_password',
                  onPressed: () {}),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildTextField({
    BuildContext context,
    String title,
    TextEditingController controller,
  }) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          MyText.sTextBold(context: context, text: title),
          TextFormField(
            decoration: InputDecoration(
              hintStyle: TextStyle(color: accentColor),
              hintText: appLocalizations.getMessageByLangAndKey(
                  appLanguageCode, title),
            ),
            controller: controller,
            cursorColor: primaryColor,
          ),
          SizedBox(
            height: 20,
          ),
        ],
      ),
    );
  }
}
