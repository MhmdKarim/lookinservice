import 'dart:convert';
import 'dart:io';

import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:image_picker/image_picker.dart';
import 'package:lookinservice/Generics/NetworkUtil.dart';
import 'package:lookinservice/Models/api_response_model.dart';
import 'package:lookinservice/Models/branch_filter_model.dart';
import 'package:lookinservice/PreDefined/app_constants.dart';
import 'package:lookinservice/PreDefined/localization.dart';
import 'package:lookinservice/Utils/size_config.dart';
import 'package:lookinservice/Views/Auth/login_page.dart';
import 'package:lookinservice/Views/Auth/map_page.dart';
import 'package:lookinservice/Views/success_page.dart';
import 'package:lookinservice/Widgets/button.dart';
import 'package:lookinservice/Widgets/text_widgets.dart';

class BranchRegistrationPage extends StatefulWidget {
  static const tag = '/BranchRegistrationPage';

  @override
  _BranchRegistrationPageState createState() => _BranchRegistrationPageState();
}

class _BranchRegistrationPageState extends State<BranchRegistrationPage> {
  AppLocalizations appLocalizations;
  File _taxImage;
  String _taxBase64;
  String _commercialBase64;
  File _commercialImage;
  final _formKey = GlobalKey<FormState>();
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final _userNameController = TextEditingController();
  final _passwordController = TextEditingController();
  final _numberController = TextEditingController();
  final _nameArabicController = TextEditingController();
  final _nameEnglishController = TextEditingController();
  final _addressArabicController = TextEditingController();
  final _adddressEnglishController = TextEditingController();
  final _termsArabicController = TextEditingController();
  final _termsEnglishController = TextEditingController();
  final _phoneController = TextEditingController();
  final _emailController = TextEditingController();
  final RoundedLoadingButtonController _btnController =
      new RoundedLoadingButtonController();
  Countries _selectedCountry = Countries();
  Cities _selectedCity = Cities();
  Areas _selectedArea = Areas();
  String _selectedLocation = '';
  GoogleMapController _mapController;
  // bool _isLoading = false;
  List<Countries> _countriesList = [];
  List<Cities> _citiesList = [];
  List<Areas> _areasList = [];
  LatLng _center;
  List<Marker> _markers = <Marker>[];
  bool _locationSelected = false;
  final _picker = ImagePicker();

  void _onMapCreated(GoogleMapController controller) {
    _mapController = controller;
  }

  // Future<void> _openMap(double latitude, double longitude) async {
  //   String googleUrl =
  //       'https://www.google.com/maps/search/?api=1&query=$latitude,$longitude';
  //   if (await canLaunch(googleUrl)) {
  //     await launch(googleUrl);
  //   } else {
  //     throw 'Could not open the map.';
  //   }
  // }

  _markerDraw(LatLng center) {
    _markers.add(
      Marker(
        markerId: MarkerId('SomeId'),
        position: center,
        //infoWindow: InfoWindow(title: _profileDataModel.username),
        // onTap: () {}
      ),
    );
    _mapController.moveCamera(CameraUpdate.newLatLng(_center));
  }

  @override
  void initState() {
    _selectedCountry.name = '';
    _selectedCity.name = '';
    _selectedArea.name = '';
    _center = LatLng(30,
        31.1884234); //TODO to be changed with data returned from area selection
    _getCountryCityArea();
    super.initState();
  }

  @override
  void dispose() {
    _userNameController.dispose();
    _passwordController.dispose();
    _numberController.dispose();
    _nameArabicController.dispose();
    _nameEnglishController.dispose();
    _addressArabicController.dispose();
    _adddressEnglishController.dispose();
    _termsArabicController.dispose();
    _termsEnglishController.dispose();
    _phoneController.dispose();
    _emailController.dispose();
    
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig.init(context);
    appLocalizations = AppLocalizations.of(context);
    return Directionality(
      textDirection: isRightToLeft ? TextDirection.rtl : TextDirection.ltr,
      child: Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
            centerTitle: true,
            elevation: 0,
            backgroundColor: primaryColorLight,
            title: FittedBox(
              child: MyText.mTextBold(
                context: context,
                text: 'Trade_mark_branch_registration',
              ),
            )),
        body: SingleChildScrollView(
          child: Form(
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: MyText.sText(
                      context: context,
                      text: 'Now_You_are_registerating_your_trade_mark__'),
                ),
                _buildTextField(
                    context: context,
                    title: 'No_of_trade_mark_you_follow',
                    hint: 'No_of_trade_mark_you_follow',
                    controller: _numberController),
                _buildTextField(
                    context: context,
                    title: 'User_name',
                    hint: 'User_name',
                    controller: _userNameController),
                _buildTextField(
                    obscureText: true,
                    context: context,
                    title: 'Password',
                    hint: 'Password',
                    controller: _passwordController),
                _buildTextField(
                    context: context,
                    title: 'Branch_name_in_Arabic',
                    hint: 'Branch_name_in_Arabic',
                    controller: _nameArabicController),
                _buildTextField(
                    context: context,
                    title: 'Branch_name_in_English',
                    hint: 'Branch_name_in_English',
                    controller: _nameEnglishController),
                _buildListTile(
                  context: context,
                  title: 'Country',
                  selected: _selectedCountry.name,
                  function: () {
                    setState(() {
                      //_categoryChips.clear();
                    });
                    _showSheet(
                      context,
                      _sheet(
                        title: 'Country',
                        length: _countriesList.length,
                        function: (context, index) {
                          return _buildCountryItem(
                              context: context, index: index);
                        },
                      ),
                    );
                  },
                ),
                _buildListTile(
                  context: context,
                  title: 'City',
                  selected: _selectedCity.name,
                  function: () {
                    setState(() {
                      //_categoryChips.clear();
                      //_categoryChipsId.clear();
                    });
                    _showSheet(
                      context,
                      _sheet(
                        title: 'City',
                        length: _citiesList.length,
                        function: (context, index) {
                          return _buildCityItem(context: context, index: index);
                        },
                      ),
                    );
                  },
                ),
                _buildListTile(
                  context: context,
                  title:
                      'District', // TODO ask sayed to return lat long of district to open map with it
                  selected: _selectedArea.name,
                  function: () {
                    setState(() {
                      //_categoryChips.clear();
                      //_categoryChipsId.clear();
                    });
                    _showSheet(
                      context,
                      _sheet(
                        title: 'District',
                        length: _areasList.length,
                        function: (context, index) {
                          return _buildAreaItem(context: context, index: index);
                        },
                      ),
                    );
                  },
                ),
                _locationSelected ? _buildMap() : _buildLocationListTile(),
                SizedBox(height: 20),
                _buildTextField(
                    context: context,
                    title: 'Branch_address_in_Arabic',
                    hint: 'Branch_address_in_Arabic',
                    controller: _addressArabicController),
                _buildTextField(
                    context: context,
                    title: 'Branch_address_in_English',
                    hint: 'Branch_address_in_English',
                    controller: _adddressEnglishController),
                _buildTextField(
                    context: context,
                    title: 'Phone_Number',
                    hint: 'Phone_Number',
                    controller: _phoneController),
                _buildTextField(
                    context: context,
                    title: 'email',
                    hint: 'email',
                    controller: _emailController),
                Container(
                  height: vBlock * 30,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    //scrollDirection: Axis.horizontal,
                    children: <Widget>[
                      _documentItem(
                          title: 'tax_card',
                          function: () {
                            _pickImageTx(ImageSource.gallery);
                          },
                          file: _taxImage),
                      _documentItem(
                          title: 'commercial_record',
                          function: () {
                            _pickImageCom(ImageSource.gallery);
                          },
                          file: _commercialImage),
                      // _documentItemCom(title: 'commercial_record'),
                    ],
                  ),
                ),
                _buildTextField(
                    context: context,
                    title: 'Terms_conditions_in_Arabic',
                    hint: 'Terms_conditions_in_Arabic',
                    controller: _termsArabicController,
                    maxLines: null),
                _buildTextField(
                    context: context,
                    title: 'Terms_conditions_in_English',
                    hint: 'Terms_conditions_in_English',
                    controller: _termsEnglishController,
                    maxLines: null),
                Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: RoundedLoadingButton(
                    color: primaryColor,
                    width: hBlock * 85,
                    controller: _btnController,
                    onPressed: () {
                      if (_formKey.currentState.validate() && _validation()) {
                        _postBranchReg();
                      } else {
                        _btnController.reset();
                        _scaffoldKey.currentState.showSnackBar(
                          SnackBar(
                            backgroundColor: Colors.red,
                            content: MyText.sText(
                              context: context,
                              text: 'Please_fill_all',
                            ),
                          ),
                        );
                      }
                    },
                    child: MyText.mText(
                        context: context,
                        text: "Trade_mark_branch_registration",
                        color: Colors.white),
                  ),
                ),
                SizedBox(
                  height: 30,
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  bool _validation() {
    if (_taxImage == null ||
        _commercialImage == null ||
        _selectedArea == null ||
        _selectedCity == null ||
        _selectedCountry == null ||
        _center == null) {
      return false;
    } else {
      return true;
    }
  }

  void _showSheet(BuildContext ctx, Widget sheet) {
    showModalBottomSheet<dynamic>(
      isScrollControlled: true,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20.0), topRight: Radius.circular(20.0)),
      ),
      context: ctx,
      builder: (context) {
        return Wrap(
          children: <Widget>[sheet],
        );
      },
    );
  }

  Widget _buildListTile(
      {BuildContext context,
      String title,
      Function function,
      String selected}) {
    return Column(
      children: <Widget>[
        ListTile(
            title: selected == ''
                ? MyText.sText(
                    context: context,
                    text: title,
                  )
                : MyText.sTextNL(
                    context: context,
                    text: selected,
                  ),
            trailing: Icon(
              Icons.arrow_forward_ios,
              size: 20,
            ),
            onTap: function),
        Container(
          height: 1,
          width: hBlock * 95,
          color: accentColor,
        )
      ],
    );
  }

  void updateCenter(LatLng newCenter) {
    setState(() {
      _center = newCenter;
      _markerDraw(_center);
    });
    print('newCenter: ' + newCenter.toString());
    print('_center: ' + _center.toString());
  }

  void moveToMapPage() async {
    final newCenter = await Navigator.push(
      context,
      MaterialPageRoute(
          fullscreenDialog: true,
          builder: (context) => MapPage(
                center: _center,
              )),
    );
    updateCenter(newCenter);
    print('updateCenter: ' + newCenter.toString());
  }

  Widget _buildMap() {
    // _mapController.moveCamera(CameraUpdate.newLatLng(_center));
    return Column(
      children: <Widget>[
        Row(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10),
              child: MyText.sText(
                  context: context, text: 'Locate_your_place_on_the_map'),
            ),
            Spacer(),
            IconButton(
                icon: Icon(Icons.edit),
                color: primaryColor,
                onPressed: () {
                  moveToMapPage();
                  //  Navigator.push(
                  //   context,
                  //   MaterialPageRoute(
                  //     builder: (context) => MapPage(
                  //       center: _center,
                  //     ),
                  //   ),
                  // );
                  // _openMap(_center.latitude, _center.longitude);
                }),
            // IconButton(
            //     icon: Icon(Icons.delete),
            //     color: Colors.red,
            //     onPressed: () {
            //       setState(() {
            //         _markers.clear();
            //       });
            //     })
          ],
        ),
        Container(
          margin: const EdgeInsets.all(8.0),
          height: 150,
          child: ClipRRect(
            borderRadius: BorderRadius.circular(10.0),
            child: GoogleMap(
              markers: Set<Marker>.of(_markers),
              onMapCreated: _onMapCreated,
              initialCameraPosition: CameraPosition(
                target: _center,
                zoom: 11.0,
              ),
            ),
          ),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            border: Border.all(
              width: 1,
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildLocationListTile() {
    return Column(
      children: <Widget>[
        ListTile(
            title: _selectedLocation == ''
                ? MyText.sText(
                    context: context,
                    text: 'Locate_your_place_on_the_map',
                  )
                : MyText.sTextNL(
                    context: context,
                    text: _selectedLocation,
                  ),
            trailing: Icon(
              Icons.location_on,
              color: primaryColor,
              size: 20,
            ),
            onTap: () {
              _locationSelected = true;
              moveToMapPage();
              // openMap(_center.latitude, _center.longitude);
            }),
        Container(
          height: 1,
          width: hBlock * 95,
          color: accentColor,
        )
      ],
    );
  }

  Widget _buildCountryItem({BuildContext context, int index}) {
    return Column(
      children: <Widget>[
        ListTile(
          title: Text(_countriesList[index].name),
          trailing: Icon(Icons.arrow_forward_ios),
          onTap: () {
            Navigator.of(context).pop();
            print(_countriesList[index].id);
            setState(() {
              _selectedCountry = _countriesList[index];
            });
          },
        ),
        Container(height: 1, width: double.infinity, color: primaryColor)
      ],
    );
  }

  Widget _buildCityItem({BuildContext context, int index}) {
    return Column(
      children: <Widget>[
        ListTile(
            title: Text(_citiesList[index].name),
            trailing: Icon(Icons.arrow_forward_ios),
            onTap: () {
              Navigator.of(context).pop();
              print(_citiesList[index].name);
              setState(() {
                _selectedCity = _citiesList[index];
              });
            }),
        Container(
          height: 1,
          width: double.infinity,
          color: accentColor,
        )
      ],
    );
  }

  Widget _buildAreaItem({BuildContext context, int index}) {
    return Column(
      children: <Widget>[
        ListTile(
            title: Text(_areasList[index].name),
            trailing: Icon(Icons.arrow_forward_ios),
            onTap: () {
              Navigator.of(context).pop();
              print(_areasList[index].name);
              setState(() {
                _selectedArea = _areasList[index];
              });
            }),
        Container(
          height: 1,
          width: double.infinity,
          color: accentColor,
        )
      ],
    );
  }

  Widget _sheet({String title, int length, Function function}) {
    return Directionality(
      textDirection: isRightToLeft ? TextDirection.rtl : TextDirection.ltr,
      child: StatefulBuilder(
          builder: (BuildContext context, StateSetter setState) {
        return SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.only(
              top: 10,
              left: 10,
              right: 10,
              bottom: MediaQuery.of(context).viewInsets.bottom + 10,
            ),
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Center(
                      child: Container(
                        height: 5,
                        width: 50,
                        color: accentColor,
                      ),
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      SizedBox(
                        width: 46,
                      ),
                      MyText.lText(context: context, text: title),
                      IconButton(
                        icon: Icon(Icons.close),
                        iconSize: 30,
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                      ),
                    ],
                  ),
                  ListView.builder(
                    scrollDirection: Axis.vertical,
                    physics: ScrollPhysics(),
                    shrinkWrap: true,
                    itemCount: length,
                    itemBuilder: function,
                  ),
                  // Btn.button(context: context, title: 'Done', onPressed: () {})
                ],
              ),
            ),
          ),
        );
      }),
    );
  }

  _pickImageTx(ImageSource _imageSource) async {
    PickedFile pickedFile = await _picker.getImage(source: _imageSource);
    final File _file = File(pickedFile.path);
    List<int> imageBytes = _file.readAsBytesSync();
    _taxBase64 = base64Encode(imageBytes);
    print(_taxBase64);
    if (_file != null) {
      setState(() {
        _taxImage = _file;
      });
    }
  }

  _pickImageCom(ImageSource _imageSource) async {
    PickedFile pickedFile = await _picker.getImage(source: _imageSource);
    final File _file = File(pickedFile.path);
    List<int> imageBytes = _file.readAsBytesSync();
    _commercialBase64 = base64Encode(imageBytes);
    print(_commercialBase64);
    if (_file != null) {
      setState(() {
        _commercialImage = _file;
      });
    }
  }

  Widget _buildTextField(
      {BuildContext context,
      String title,
      String hint,
      TextEditingController controller,
      bool obscureText = false,
      int maxLines = 1}) {
    return Padding(
      padding: const EdgeInsets.all(8),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          MyText.sText(context: context, text: title),
          TextFormField(
              obscureText: obscureText,
              maxLines: maxLines,
              decoration: InputDecoration(
                hintStyle: TextStyle(color: accentColor),
                hintText: appLocalizations.getMessageByLangAndKey(
                    appLanguageCode, hint),
              ),
              controller: controller,
              cursorColor: primaryColor,
              validator: (value) {
                if (value.isEmpty) {
                  _btnController.reset();
                  return appLocalizations.getMessageByLangAndKey(
                          appLanguageCode, 'Enter') +
                      ' ' +
                      appLocalizations.getMessageByLangAndKey(
                          appLanguageCode, title);
                } else {
                  return null;
                }
              }),
          SizedBox(
            height: 10,
          ),
        ],
      ),
    );
  }

  Widget _documentItem({String title, Function function, File file}) {
    return Padding(
      padding: EdgeInsets.all(hBlock * 2),
      child: DottedBorder(
        color: accentColor,
        borderType: BorderType.RRect,
        radius: Radius.circular(12),
        padding: EdgeInsets.all(hBlock * 2),
        child: ClipRRect(
          borderRadius: BorderRadius.all(Radius.circular(12)),
          child: Container(
            width: hBlock * 40,
            height: vBlock * 50,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                InkWell(
                  onTap: function,
                  child: file == null
                      ? Container(
                          height: hBlock * 25,
                          child: Image.asset('assets/add-image.png',
                              fit: BoxFit.fitHeight),
                        )
                      : Container(
                          decoration: BoxDecoration(
                              color: accentColor,
                              image: DecorationImage(
                                  image: FileImage(file), fit: BoxFit.cover)),
                          height: vBlock * 20,
                          width: hBlock * 30,
                        ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(vertical: hBlock * 2),
                  child: MyText.sText(context: context, text: title),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  _getCountryCityArea() async {
    ApiResponseModel responseModel = await NetworkUtil.getWithoutToken(
      'branchFilter',
    );
    if (responseModel == null) {
      return;
    }
    if (responseModel.statuscode == 200) {
      BranchFilterModel model = BranchFilterModel.fromJson(responseModel.data);
      if (model == null) {
        return;
      }
      _countriesList = model.countries;
      _citiesList = model.cities;
      _areasList = model.areas;
      setState(() {
        // _isLoading = false;
      });
    } else {
      //_isLoading = false;
    }
  }

  _postBranchReg() async {
    Map<String, dynamic> jsonMap = {
      "address_ar": _addressArabicController.text,
      "address_en": _adddressEnglishController.text,
      "username": _userNameController.text,
      "phone": _phoneController.text,
      "email": _emailController.text,
      "password": _passwordController.text,
      "longitude": _center.latitude,
      "latitude": _center.longitude,
      "area_id": _selectedArea.id,
      "city_id": _selectedCity.id,
      "country_id": _selectedCountry.id,
      "main_store_id": _numberController.text,
      "tax_card": _taxBase64,
      "commercial_record": _commercialBase64,
    };
    String jsonString = json.encode(jsonMap);
    ApiResponseModel responseModel = await NetworkUtil.postWithoutToken(
      'branchRegister',
      jsonString,
    );
    if (responseModel == null) {
      return;
    }
    print(responseModel.statuscode);
    print(responseModel.message);

    if (responseModel.statuscode == 200) {
      print(responseModel.message);
      _btnController.success();
      Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => SuccessPage(
              appBarTitle: 'Registration_confirmation',
              title: 'Your_request_has_been_sent',
              text: 'Brand_request_sent_to_admin___',
              btnText: 'Back_to_registration',
              onPressed: () {
                Navigator.of(context).pushReplacementNamed(LoginPage.tag);
              },
              visible: true,
            ),
          ));
    } else {
      _btnController.reset();
    }
  }
}
