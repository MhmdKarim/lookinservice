import 'package:flutter/material.dart';
import 'package:lookinservice/PreDefined/app_constants.dart';
import 'package:lookinservice/Utils/size_config.dart';
import 'package:lookinservice/Widgets/standard_button_widget.dart';
import 'package:lookinservice/Widgets/text_widgets.dart';

class SuccessPage extends StatelessWidget {
  static const tag = '/SuccessPage';
  final String appBarTitle;
  final String title;
  final String text;
  final String btnText;
  final Function onPressed;
  final bool visible;

  SuccessPage(
      {this.appBarTitle,
      this.title,
      this.text,
      this.btnText,
      this.visible,
      this.onPressed});

  @override
  Widget build(BuildContext context) {
    SizeConfig.init(context);
    // AppLocalizations  appLocalizations = AppLocalizations.of(context);
    return Directionality(
      textDirection: isRightToLeft ? TextDirection.rtl : TextDirection.ltr,
      child: Scaffold(
        appBar: AppBar(
            centerTitle: true,
            elevation: 0,
            backgroundColor: primaryColorLight,
            title: MyText.mTextBold(
              context: context,
              text: appBarTitle, //'Registration_confirmation',
            )),
        body: Center(
          child: Column(//TODO RENDERFLEX
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(20.0),
                child: Image.asset(
                  'assets/success.png',
                  width: hBlock * 25,
                ),
              ),
              MyText.mTextBold(
                  context: context, text: title //'Your_request_has_been_sent'
                  ),
              Padding(
                padding: const EdgeInsets.all(20.0),
                child: MyText.sText(
                  context: context,
                  text: text, //'Your_request_has_been_sent_to_admin___',
                ),
              ),
              Visibility(
                  visible: visible,
                  child: Btn.button(
                    context: context,
                    title: btnText, //'Back_to_registration',
                    onPressed: onPressed,
                  ))
            ],
          ),
        ),
      ),
    );
  }
}
