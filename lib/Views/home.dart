import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:lazy_load_scrollview/lazy_load_scrollview.dart';
import 'package:lookinservice/Generics/NetworkUtil.dart';
import 'package:lookinservice/Models/api_response_model.dart';
import 'package:lookinservice/Models/home_model.dart';
import 'package:lookinservice/Models/reasons_model.dart';
import 'package:lookinservice/PreDefined/app_constants.dart';
import 'package:lookinservice/PreDefined/localization.dart';
import 'package:lookinservice/Utils/size_config.dart';
import 'package:lookinservice/Views/Messages/chat_page.dart';
import 'package:lookinservice/Widgets/button.dart';
import 'package:lookinservice/Widgets/custom_appbar_widget.dart';
import 'package:lookinservice/Widgets/no_data_widget.dart';
import 'package:lookinservice/Widgets/text_widgets.dart';
import 'package:lookinservice/Widgets/user_photo.dart';

class HomePage extends StatefulWidget {
  static const tag = '/HomePage';

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  AppLocalizations appLocalizations;
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final RoundedLoadingButtonController _btnController =
      new RoundedLoadingButtonController();
  final _noticeController = TextEditingController();
  bool _isLoading = false;
  List<HomeDataModel> _homeDataModel = [];
  ReasonsDataModel _selectedReason;
  List<ReasonsDataModel> _allReasons = [];

  int _page = 1;
  int _lastPage = 1;

  @override
  void initState() {
    _getData();
    _getReasonsData();
    super.initState();
  }

  @override
  void dispose() {
    _noticeController.dispose();
    super.dispose();
  }

  Future<void> _refreshData() async {
 if (_page ==1){
   _homeDataModel.clear();

 }
    setState(() {
      print(' I am getting data');
      //  Future.delayed(Duration(seconds: 0, milliseconds: 1000));
      _getData();
    });

  }

  @override
  Widget build(BuildContext context) {
    SizeConfig.init(context);
    appLocalizations = AppLocalizations.of(context);
    return SafeArea(
      child: Directionality(
        textDirection: isRightToLeft ? TextDirection.rtl : TextDirection.ltr,
        child: Scaffold(
          key: _scaffoldKey,
          // appBar: CustomAppBar.homeAndMoreAppBar(context),
          body: Stack(
            children: <Widget>[
              Positioned.directional(
                  textDirection:
                      isRightToLeft ? TextDirection.rtl : TextDirection.ltr,
                  top: 0.0,
                  start: 0.0,
                  end: 0.0,
                  child: CustomAppBar.homeAndMoreAppBar(context)),
              Container(
                padding: EdgeInsets.only(
                    top: 80, left: hBlock * 2, right: hBlock * 2),
                child: _isLoading
                    ? Center(
                        child: CircularProgressIndicator(),
                      )
                    : _homeDataModel.length == 0
                        ? NoDataAvailable(
                            image: 'assets/reservation-empty.png',
                            text: 'Sorry_no_requests_available',
                            visible: true,
                            onPressed: _refreshData,
                          )
                        : LazyLoadScrollView(
                            scrollOffset: 0,
                            onEndOfPage: _refreshData,
                            child: RefreshIndicator(
                              onRefresh: _refreshData,
                              child: ListView.builder(
                                 physics: ScrollPhysics(),
                                shrinkWrap: true,
                                  scrollDirection: Axis.vertical,
                                  itemCount: _homeDataModel.length,
                                  itemBuilder: (context, index) {
                                    return _buildItems(index, _homeDataModel);
                                  }),
                            ),
                          ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildItems(int index, List<HomeDataModel> list) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Container(
          margin: EdgeInsets.only(
              top: hBlock * 2, left: hBlock * 2, right: hBlock * 2),
          //height: vBlock * 25,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10.0),
            border: Border.all(
              color: accentColor,
              width: 1,
            ),
          ),
          child: Column(
            children: <Widget>[
              Row(
                children: <Widget>[
                  Container(
                    // height: 200,
                    width: hBlock * 25,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        UserPhoto(
                          height: hBlock * 20,
                          imageUrl: list[index].user.image,
                        ),
                        InkWell(
                            onTap: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => ChatPage(
                                    title: list[index].user.name,
                                    id: list[index].id,
                                    visible: true,
                                  ),
                                ),
                              );
                            },
                            child: CircleAvatar(
                              backgroundImage:
                                  AssetImage('assets/send-message.png'),
                            )),
                      ],
                    ),
                    //child: UserPhoto(),
                  ),
                  Container(
                    padding: EdgeInsets.only(top: 8, left: 8, right: 8),
                    width: hBlock * 65,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 5),
                          child: MyText.sTextNLBold(
                              context: context, text: list[index].user.name),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 5),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              MyText.sText(
                                context: context,
                                text: 'Booking_Date',
                              ),
                              MyText.sTextNL(
                                context: context,
                                text: list[index].date,
                              ),
                              // Text(list[index].date),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 5),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              MyText.sText(
                                context: context,
                                text: 'Time',
                              ),
                              MyText.sTextNL(
                                context: context,
                                text: list[index].time,
                              ),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 5),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              MyText.sText(
                                context: context,
                                text: 'No_Persons',
                              ),
                              MyText.sTextNL(
                                context: context,
                                text: list[index].slotNo.toString(),
                              ),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 5),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              MyText.sText(
                                context: context,
                                text: 'Booking_notice',
                              ),
                              Spacer(),
                              Flexible(
                                child: MyText.sTextNL(
                                  context: context,
                                  text: list[index].note,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  _button(
                      context: context,
                      color: Colors.green,
                      title: 'Approve_Request',
                      onPressed: () {
                        print('dfdf  ' + _homeDataModel[index].id.toString());
                        setState(() {
                          _postChangeStatusAccept(index);
                        });
                      }),
                  _button(
                      context: context,
                      color: Colors.red,
                      title: 'Reject_Request',
                      onPressed: () {
                        _showRejectionPopUp(context, index);
                      }),
                ],
              )
            ],
          ),
        ),
      ],
    );
  }

  Widget _button(
      {BuildContext context,
      int index,
      // int id,
      String title,
      Color color,
      Function onPressed}) {
    return Container(
      margin: EdgeInsets.all(hBlock * 2),
      width: hBlock * 40,
      height: 50,
      child: RaisedButton(
          color: color,
          elevation: 2,
          onPressed: onPressed,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10),
          ),
          child: MyText.sTextBold(
            context: context,
            text: title,
            color: Colors.white,
          )),
    );
  }

  _showRejectionPopUp(BuildContext context, int index) {
    AppLocalizations appLocalizations = AppLocalizations.of(context);
    showDialog(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return StatefulBuilder(
          builder: (BuildContext context, StateSetter setState) {
            return Directionality(
              textDirection:
                  isRightToLeft ? TextDirection.rtl : TextDirection.ltr,
              child: SimpleDialog(
                contentPadding: MediaQuery.of(context).viewInsets +
                    const EdgeInsets.symmetric(
                        horizontal: 20.0, vertical: 20.0),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10)),
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      SizedBox(
                        width: 30,
                      ),
                      Text(
                        appLocalizations.getMessageByLangAndKey(
                            appLanguageCode, 'Request_rejection'),
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontSize: 20,
                        ),
                      ),
                      IconButton(
                        iconSize: 30,
                        icon: Icon(Icons.close),
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                      ),
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: DropdownButton(
                      iconSize: 30,
                      isExpanded: true,
                      hint: MyText.mText(
                          context: context, text: "Select_rejection_reason"),
                      value: _selectedReason,
                      items: _allReasons.map((value) {
                        return DropdownMenuItem(
                          child: MyText.mTextNL(
                              context: context, text: value.reason),
                          value: value,
                        );
                      }).toList(),
                      onChanged: (value) {
                        setState(() {
                          _selectedReason = value;
                        });
                      },
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: TextFormField(
                      controller: _noticeController,
                      textInputAction: TextInputAction.newline,
                      keyboardType: TextInputType.multiline,
                      maxLines: 4,
                      decoration: InputDecoration(
                        labelText: appLocalizations.getMessageByLangAndKey(
                            appLanguageCode, 'Write_your_notes'),
                        hintText: appLocalizations.getMessageByLangAndKey(
                            appLanguageCode, 'Write_your_notes'),
                        hintStyle: TextStyle(
                          fontSize: hBlock * 5,
                        ),
                        labelStyle: TextStyle(
                          fontSize: hBlock * 5,
                        ),
                      ),
                    ),
                  ),
                  IgnorePointer(
                    ignoring: _selectedReason == null,
                    child: Container(
                      margin: EdgeInsets.all(8),
                      width: hBlock * 85,
                      height: 60,
                      child: RoundedLoadingButton(
                        color:
                            _selectedReason == null ? accentColor : Colors.red,
                        width: hBlock * 85,
                        controller: _btnController,
                        onPressed: () {
                          setState(() {
                            _postChangeStatusReject(index);
                          });
                        },
                        child: MyText.mText(
                            context: context,
                            text: 'Request_rejection',
                            color: Colors.white),
                      ),
                    ),
                  ),
                ],
              ),
            );
          },
        );
      },
    );
  }

  _getData() async {
    _isLoading = true;
    ApiResponseModel responseModel = await NetworkUtil.get(
      'bookings?page=$_page',
    );
    if (responseModel == null) {
      return;
    }
    if (responseModel.statuscode == 200) {
      HomeModel model = HomeModel.fromJson(responseModel.data);
      if (model == null) {
        return;
      }
      print(responseModel.data);
      _lastPage = model.lastPage;
      if (model.nextPageUrl != null) {
        _page++;
      }
      _homeDataModel.addAll(model.data);

      print(model.data);
      if (mounted) {
        setState(() {
          _isLoading = false;
        });
      }
    } else {
      _isLoading = false;
    }
  }

  _getReasonsData() async {
    _isLoading = true;
    ReasonsModel responseModel = await NetworkUtil.getReasons(
      'reasons',
    );
    if (responseModel == null) {
      return;
    }
    if (responseModel.statusCode == 200) {
      _allReasons = responseModel.data;
      if (_allReasons == null) {
        return;
      }
      _allReasons.forEach((f) {
        print(f.id);
      });
      if (mounted) {
        setState(() {
          _isLoading = false;
        });
      }
    } else {
      _isLoading = false;
    }
  }

  _postChangeStatusReject(int index) async {
    Map<String, dynamic> jsonMap = {
      "status": 6,
      "booking_id": _homeDataModel[index].id,
      "reason_id": _selectedReason.id,
      "note": _noticeController.text == null ? '' : _noticeController.text
    };
    String jsonString = json.encode(jsonMap);
    ApiResponseModel responseModel = await NetworkUtil.post(
      'bookings',
      jsonString,
    );
    if (responseModel == null) {
      return;
    }
    if (responseModel.statuscode == 200) {
      setState(() {
        _homeDataModel.removeAt(index);
      });
      Navigator.of(context).pop();
      _scaffoldKey.currentState.showSnackBar(SnackBar(
        content: Text(
            appLocalizations.getMessageByLangAndKey(
                appLanguageCode, 'Request_rejection'),
            textAlign: TextAlign.center),
        backgroundColor: Colors.red,
      ));
    } else {
      return;
    }
  }

  _postChangeStatusAccept(int index) async {
    Map<String, dynamic> jsonMap = {
      "status": 2,
      "booking_id": _homeDataModel[index].id,
      "reason_id": null,
      "note": null
    };
    String jsonString = json.encode(jsonMap);
    ApiResponseModel responseModel = await NetworkUtil.post(
      'bookings',
      jsonString,
    );
    if (responseModel == null) {
      return;
    }

    if (responseModel.statuscode == 200) {
      setState(() {
        _homeDataModel.removeAt(index);
      });
      _scaffoldKey.currentState.showSnackBar(SnackBar(
        content: Text(
            appLocalizations.getMessageByLangAndKey(
                appLanguageCode, 'Your_booking_has_been_approved___'),
            textAlign: TextAlign.center),
        backgroundColor: primaryColor,
      ));
    } else {
      return;
    }
  }
}
