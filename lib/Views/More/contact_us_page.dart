import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:lookinservice/Generics/NetworkUtil.dart';
import 'package:lookinservice/Models/api_response_model.dart';
import 'package:lookinservice/Views/success_page.dart';
import '../../PreDefined/app_constants.dart';
import '../../PreDefined/localization.dart';
import '../../Utils/size_config.dart';
import '../../Widgets/custom_appbar_widget.dart';
import '../../Widgets/button.dart';
import '../../Widgets/text_widgets.dart';

class ContactUsPage extends StatefulWidget {
  static const tag = '/ContactUsPage';

  @override
  _ContactUsPageState createState() => _ContactUsPageState();
}

class _ContactUsPageState extends State<ContactUsPage> {
  // GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  AppLocalizations appLocalizations;
  final _nameController = TextEditingController();
  final _phoneController = TextEditingController();
  final _messageController = TextEditingController();
  final RoundedLoadingButtonController _btnController =
      new RoundedLoadingButtonController();

  @override
  void dispose() {
    _nameController.dispose();
    _phoneController.dispose();
    _messageController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig.init(context);
    appLocalizations = AppLocalizations.of(context);
    return Directionality(
      textDirection: isRightToLeft ? TextDirection.rtl : TextDirection.ltr,
      child: Scaffold(
        appBar: CustomAppBar.appBar(context, "contact_us", null),
        body: ListView(
          // mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: Center(
                      child: Image.asset(
                        'assets/support-page.png',
                        fit: BoxFit.contain,
                      ),
                    ),
                  ),
                  MyText.mText(
                    context: context,
                    text: 'Name',
                    color: accentColor,
                  ),
                  TextFormField(
                    controller: _nameController,
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  MyText.mText(
                    context: context,
                    text: 'Phone_Number',
                    color: accentColor,
                  ),
                  TextFormField(
                    controller: _phoneController,
                    keyboardType: TextInputType.phone,
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  MyText.mText(
                    context: context,
                    text: 'Write_your_Request',
                    color: accentColor,
                  ),
                  TextFormField(
                    controller: _messageController,
                    maxLines: 5,
                  )
                ],
              ),
            ),
            RoundedLoadingButton(
              // curve: Curves.ease,
              //width: screenWidth * .6,
              color: primaryColor,
              width: hBlock * 85,
              controller: _btnController,
              onPressed: () {
                postContactUs();
                _btnController.reset();
              },
              child: MyText.mText(
                  context: context, text: "contact_us", color: Colors.white),
            ),
            SizedBox(height: 20)
          ],
        ),
      ),
    );
  }

  postContactUs() async {
    Map<String, dynamic> jsonMap = {
      "name": _nameController.text,
      "phone": _phoneController.text,
      "msg": _messageController.text
    };
    String jsonString = json.encode(jsonMap);
    ApiResponseModel responseModel = await NetworkUtil.postWithoutToken(
      'contactUs',
      jsonString,
    );
    if (responseModel == null) {
      return;
    }
    if (responseModel.statuscode == 200) {
      print(responseModel.message);
      _btnController.success();
      setState(() {
        Navigator.of(context).pop();
        Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => SuccessPage(
                appBarTitle: 'contact_us',
                title: 'Message_sent_successfully',
                text: 'Contact_you_soon',
                btnText: 'Back_to_registration',
                onPressed: () {},
                visible: false,
              ),
            ));
      });
    } else {
      _btnController.reset();
    }
  }
}
// {
//     "name": "Sayed Admin",
//     "phone": "123456",
//     "msg": "try to be special"
// }
