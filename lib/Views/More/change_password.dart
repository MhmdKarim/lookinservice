import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:lookinservice/Generics/NetworkUtil.dart';
import 'package:lookinservice/Models/api_response_model.dart';
import 'package:lookinservice/PreDefined/app_constants.dart';
import 'package:lookinservice/PreDefined/localization.dart';
import 'package:lookinservice/Utils/size_config.dart';
import 'package:lookinservice/Widgets/custom_appbar_widget.dart';
import 'package:lookinservice/Widgets/button.dart';
import 'package:lookinservice/Widgets/text_widgets.dart';

class ChangePasswordPage extends StatefulWidget {
  static const tag = '/ChangePasswordPage';
  @override
  _ChangePasswordPageState createState() => _ChangePasswordPageState();
}

class _ChangePasswordPageState extends State<ChangePasswordPage> {
  AppLocalizations appLocalizations;
  final _currentPasswordController = TextEditingController();
  final _newPasswordController = TextEditingController();
  final _confirmPasswordController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  var _scaffoldKey = GlobalKey<ScaffoldState>();

  final RoundedLoadingButtonController _btnController =
      new RoundedLoadingButtonController();

  @override
  void dispose() {
    _currentPasswordController.dispose();
    _newPasswordController.dispose();
    _confirmPasswordController.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig.init(context);
    appLocalizations = AppLocalizations.of(context);
    return Directionality(
      textDirection: isRightToLeft ? TextDirection.rtl : TextDirection.ltr,
      child: Scaffold(
        key: _scaffoldKey,
        appBar: CustomAppBar.appBar(context, 'Change_password', null),
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Form(
              key: _formKey,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  _buildTextField(
                      context: context,
                      title: 'Current_password',
                      controller: _currentPasswordController,
                      validator: (value) {
                        if (value.isEmpty) {
                          _btnController.reset();
                          return appLocalizations.getMessageByLangAndKey(
                                  appLanguageCode, 'Enter') +
                              ' ' +
                              appLocalizations.getMessageByLangAndKey(
                                  appLanguageCode, 'Current_password');
                        } else {
                          return null;
                        }
                      }),
                  _buildTextField(
                      context: context,
                      title: 'New_password',
                      controller: _newPasswordController,
                      validator: (value) {
                        if (value.isEmpty) {
                          _btnController.reset();
                          return appLocalizations.getMessageByLangAndKey(
                                  appLanguageCode, 'Enter') +
                              ' ' +
                              appLocalizations.getMessageByLangAndKey(
                                  appLanguageCode, 'New_password');
                        } else {
                          return null;
                        }
                      }),
                  _buildTextField(
                    context: context,
                    title: 'New_password_confirmation',
                    controller: _confirmPasswordController,
                    validator: (value) {
                      if (_newPasswordController.text !=
                          _confirmPasswordController.text) {
                        _btnController.reset();
                        return appLocalizations.getMessageByLangAndKey(
                            appLanguageCode, 'Password_and_Confirm');
                      } else {
                        return null;
                      }
                    },
                  ),
                  RoundedLoadingButton(
                    color: primaryColor,
                    width: hBlock * 85,
                    controller: _btnController,
                    onPressed: () {
                      if (_formKey.currentState.validate()) {
                        _postChangePassword(context);
                      }
                    },
                    child: MyText.mText(
                        context: context,
                        text: "Change_password",
                        color: Colors.white),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildTextField(
      {BuildContext context,
      String title,
      TextEditingController controller,
      Function validator}) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        MyText.mText(context: context, text: title),
        TextFormField(
          obscureText: true,
          decoration: InputDecoration(
            hintStyle: TextStyle(color: accentColor),
            hintText:
                appLocalizations.getMessageByLangAndKey(appLanguageCode, title),
          ),
          controller: controller,
          cursorColor: primaryColor,
          validator: validator,
        ),
        SizedBox(
          height: 20,
        ),
      ],
    );
  }

  _postChangePassword(BuildContext context) async {
    Map<String, dynamic> jsonMap = {
      "old_password": _currentPasswordController.text,
      "new_password": _newPasswordController.text
    };
    String jsonString = json.encode(jsonMap);
    ApiResponseModel responseModel = await NetworkUtil.post(
      'changePassword',
      jsonString,
    );
    print(responseModel.message);
    if (responseModel == null) {
      return;
    }
    if (responseModel.statuscode == 200) {
      print(responseModel.data);
      _btnController.success();

      Navigator.of(context).pop();
    } else {
      _btnController.reset();
    }
  }
}
