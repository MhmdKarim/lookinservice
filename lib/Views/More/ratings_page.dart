import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:lookinservice/Generics/NetworkUtil.dart';
import 'package:lookinservice/Models/api_response_model.dart';
import 'package:lookinservice/Models/rates_model.dart';
import 'package:lookinservice/PreDefined/app_constants.dart';
import 'package:lookinservice/PreDefined/localization.dart';
import 'package:lookinservice/Utils/size_config.dart';
import 'package:lookinservice/Widgets/custom_appbar_widget.dart';
import 'package:lookinservice/Widgets/no_data_widget.dart';
import 'package:lookinservice/Widgets/text_widgets.dart';
import 'package:lookinservice/Widgets/user_photo.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';

class RatingsPage extends StatefulWidget {
  static const tag = '/RatingsPage';
  @override
  _RatingsPageState createState() => _RatingsPageState();
}

class _RatingsPageState extends State<RatingsPage>
    with TickerProviderStateMixin {
  AppLocalizations appLocalizations;
  bool _isLoading = true;
  List<RatesDataModel> _ratingList = [];
  List<RatesDataModel> _ratingList1 = [];
  List<RatesDataModel> _ratingList2 = [];
  List<RatesDataModel> _ratingList3 = [];
  List<RatesDataModel> _ratingList4 = [];
  List<RatesDataModel> _ratingList5 = [];
  TabController _tabController;
  int _rate;

  @override
  void initState() {
    getData();
    _tabController = TabController(vsync: this, length: 5);

    super.initState();
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig.init(context);
    appLocalizations = AppLocalizations.of(context);
    return Directionality(
      textDirection: isRightToLeft ? TextDirection.rtl : TextDirection.ltr,
      child: Scaffold(
        appBar: CustomAppBar.appBar(context, "Ratings", null),
        body: _isLoading
            ? Center(child: CircularProgressIndicator())
            : SingleChildScrollView(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    _buildTopPart(context),
                    Container(
                      height: 1,
                      width: hBlock * 100,
                      color: accentColor,
                      margin: EdgeInsets.symmetric(vertical: 10),
                    ),
                    MyText.sText(context: context, text: 'Ratings'),
                    Container(
                      child: TabBar(
                        unselectedLabelColor: Colors.grey,
                        indicatorColor: primaryColor,
                        labelColor: primaryColor,
                        indicatorWeight: 2.0,
                        isScrollable: true,
                        controller: _tabController,
                        tabs: [
                          Tab(
                            child: _buildTabItem(context, 5),
                          ),
                          Tab(
                            child: _buildTabItem(context, 4),
                          ),
                          Tab(
                            child: _buildTabItem(context, 3),
                          ),
                          Tab(
                            child: _buildTabItem(context, 2),
                          ),
                          Tab(
                            child: _buildTabItem(context, 1),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      height: vBlock * 60,
                      // color: primaryColor,
                      child: TabBarView(
                        controller: _tabController,
                        children: <Widget>[
                          _ratingList5.length == 0
                              ? NoDataAvailable(
                                  image: 'assets/star-empty.png',
                                  text: 'Sorry_no_rating_available',
                                  visible: false,
                                )
                              : ListView.builder(
                                  itemCount: _ratingList5.length,
                                  itemBuilder: (context, index) {
                                    return _buildRating(index, _ratingList5);
                                  },
                                ),
                          _ratingList4.length == 0
                              ? NoDataAvailable(
                                  image: 'assets/star-empty.png',
                                  text: 'Sorry_no_rating_available',
                                  visible: false,
                                )
                              : ListView.builder(
                                  itemCount: _ratingList4.length,
                                  itemBuilder: (context, index) {
                                    return _buildRating(index, _ratingList4);
                                  },
                                ),
                          _ratingList3.length == 0
                              ? NoDataAvailable(
                                  image: 'assets/star-empty.png',
                                  text: 'Sorry_no_rating_available',
                                  visible: false,
                                )
                              : ListView.builder(
                                  itemCount: _ratingList3.length,
                                  itemBuilder: (context, index) {
                                    return _buildRating(index, _ratingList3);
                                  },
                                ),
                          _ratingList2.length == 0
                              ? NoDataAvailable(
                                  image: 'assets/star-empty.png',
                                  text: 'Sorry_no_rating_available',
                                  visible: false,
                                )
                              : ListView.builder(
                                  itemCount: _ratingList2.length,
                                  itemBuilder: (context, index) {
                                    return _buildRating(index, _ratingList2);
                                  },
                                ),
                          _ratingList1.length == 0
                              ? NoDataAvailable(
                                  image: 'assets/star-empty.png',
                                  text: 'Sorry_no_rating_available',
                                  visible: false,
                                )
                              : ListView.builder(
                                  itemCount: _ratingList1.length,
                                  itemBuilder: (context, index) {
                                    return _buildRating(index, _ratingList1);
                                  },
                                ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
      ),
    );
  }

  Row _buildTabItem(BuildContext context, int number) {
    return Row(
      children: <Widget>[
        //Icon(Icons.star, size: 10),
        MyText.sTextNL(
          context: context,
          text: '$number ',
        ),
        MyText.sText(
          context: context,
          text: 'stars',
        ),
      ],
    );
  }

  Widget _buildTopPart(BuildContext context) {
    return Container(
      width: hBlock * 100,
      child: Row(
        children: <Widget>[
          Container(
            width: hBlock * 30,
            child: Column(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(top: 10),
                  height: hBlock * 20,
                  width: hBlock * 20,
                  decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      border: Border.all(width: 1, color: accentColor)),
                  child: Center(
                      child: MyText.mTextNLBold(
                          context: context, text: _rate.toStringAsFixed(1))),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 5),
                  child: _ratingStars(_rate.toDouble()),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 5),
                  child: Text(
                    _rate.toStringAsFixed(1) +
                        ' ' +
                        appLocalizations.getMessageByLangAndKey(
                            appLanguageCode, 'out_of') +
                        ' 5',
                    style: TextStyle(fontSize: xsFont),
                  ),
                ),
                Text(
                  _ratingList.length.toString() +
                      ' ' +
                      appLocalizations.getMessageByLangAndKey(
                          appLanguageCode, 'Rating'),
                  style: TextStyle(fontSize: xsFont),
                ),
              ],
            ),
          ),
          Container(
              width: hBlock * 70,
              child: Column(
                children: <Widget>[
                  _buildLinearBar(context, 0, _ratingList5),
                  _buildLinearBar(context, 1, _ratingList4),
                  _buildLinearBar(context, 2, _ratingList3),
                  _buildLinearBar(context, 3, _ratingList2),
                  _buildLinearBar(context, 4, _ratingList1),
                ],
              ))
        ],
      ),
    );
  }

  Widget _buildLinearBar(
      BuildContext context, int index, List<RatesDataModel> list) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 10, vertical: 4),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          Text(
            '${5 - index} ' +
                appLocalizations.getMessageByLangAndKey(
                    appLanguageCode, 'stars'),
            style: TextStyle(fontSize: xsFont),
          ),
          Expanded(
            child: LinearPercentIndicator(
              lineHeight: 10.0,
              percent: list.length / _ratingList.length,
              backgroundColor: Colors.green[50],
              progressColor: primaryColor,
            ),
          ),
          Container(
            width: hBlock * 7,
            child: Text('(${list.length})',
                style: TextStyle(fontSize: xsFont, color: accentColor)),
          )
        ],
      ),
    );
  }

  Widget _ratingStars(double rate) {
    return StatefulBuilder(builder: (BuildContext context, setState) {
      return IgnorePointer(
        child: RatingBar(
          initialRating: rate, // provider.getStars.toDouble(),,
          itemSize: hBlock * 5,
          itemCount: 5,
          itemBuilder: (context, _) => Icon(
            Icons.star,
            color: Colors.orange,
          ),
          unratedColor: Colors.grey,
          textDirection: TextDirection.rtl,
          allowHalfRating: true,
          onRatingUpdate: (value) {},
        ),
      );
    });
  }

  Widget _buildRating(int index, List<RatesDataModel> list) {
    return Container(
      height: 100,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Container(height: 1, color: Colors.transparent),
          Row(
            children: <Widget>[
              UserPhoto(
                imageUrl: list[index].user.image,
                height: 50,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  MyText.sTextNLBold(
                      context: context, text: list[index].user.name),
                  _ratingStars((list[index].rate.toDouble())),
                  MyText.sTextNL(context: context, text: list[index].comment)
                ],
              ),
            ],
          ),
          Container(height: 1, color: accentColor)
        ],
      ),
    );
  }

  getData() async {
    // if(constUserLoginModel ==null){
    //   return;
    // }
    _isLoading = true;
    ApiResponseModel responseModel = await NetworkUtil.get(
      'rates',
    );
    if (responseModel == null) {
      return;
    }
    if (responseModel.statuscode == 200) {
      RatesModel model = RatesModel.fromJson(responseModel.data);
      if (model == null) {
        return;
      }

      _rate = model.rate;
      _ratingList = model.rates;
      _ratingList1 = _ratingList.where((f) => f.rate == 1).toList();
      _ratingList2 = _ratingList.where((f) => f.rate == 2).toList();
      _ratingList3 = _ratingList.where((f) => f.rate == 3).toList();
      _ratingList4 = _ratingList.where((f) => f.rate == 4).toList();
      _ratingList5 = _ratingList.where((f) => f.rate == 5).toList();

      if (mounted) {
        setState(() {
          _isLoading = false;
        });
      }
    } else {
      _isLoading = false;
    }
  }
}
