import 'package:flutter/material.dart';
import 'package:lookinservice/Generics/app_prefs.dart';
import 'package:lookinservice/Views/More/change_password.dart';
import 'package:lookinservice/Views/Settings/settings_page.dart';
import 'package:lookinservice/Views/StartPages/splash_screen.dart';
import 'package:lookinservice/Views/More/ratings_page.dart';
import 'package:lookinservice/Widgets/custom_appbar_widget.dart';
import 'package:lookinservice/Widgets/icon_widget.dart';
import '../../PreDefined/app_constants.dart';
import '../../PreDefined/localization.dart';
import '../../Utils/size_config.dart';
import 'About_us_page.dart';
import 'contact_us_page.dart';
import 'edit_profile_page.dart';
import '../../Widgets/alert_widget.dart';
import '../../Widgets/text_widgets.dart';

class MorePage extends StatefulWidget {
  @override
  _MorePageState createState() => _MorePageState();
}

class _MorePageState extends State<MorePage> {
  AppLocalizations appLocalizations;
  bool isSwitched = true;

  // _onLogin() {
  //   setState(() {});
  // }

  @override
  Widget build(BuildContext context) {
    SizeConfig.init(context);
    appLocalizations = AppLocalizations.of(context);
    return SafeArea(
          child: Directionality(
        textDirection: isRightToLeft ? TextDirection.rtl : TextDirection.ltr,
        child: Scaffold(
          //appBar: CustomAppBar.homeAndMoreAppBar(context),
          body: Stack(
            children: <Widget>[
               Positioned.directional(
                    textDirection:
                        isRightToLeft ? TextDirection.rtl : TextDirection.ltr,
                    top: 0.0,
                    start: 0.0,
                    end: 0.0,
                    child: CustomAppBar.homeAndMoreAppBar(context)),
              Container(
                 padding: EdgeInsets.only(
                      top: 80, left: hBlock * 5, right: hBlock * 5),
                child: ListView(
                  children: <Widget>[
                    Container(
                      color: primaryColorLight,
                      child: Padding(
                        padding:
                            const EdgeInsets.symmetric(vertical: 8, horizontal: 20),
                        child: MyText.mText(
                            context: context,
                            text: 'Closing_activation_of_booking___'),
                      ),
                    ),
                    Container(
                      color: primaryColorLight,
                      child: Padding(
                        padding:
                            const EdgeInsets.symmetric(vertical: 8, horizontal: 20),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            MyText.mText(
                                context: context, text: 'Bookings_are_available_now'),
                            Switch(
                              value: isSwitched,
                              onChanged: (value) {
                                setState(() {
                                  isSwitched = value;
                                  print(isSwitched); //TODO Sayed what api to use
                                });
                              },
                              activeTrackColor: accentColor,
                              activeColor: primaryColor,
                              inactiveThumbColor: canvusColor,
                              inactiveTrackColor: accentColor,
                            ),
                          ],
                        ),
                      ),
                    ),
                    _buildListTile(
                        context: context,
                        title: 'Update_Profile',
                        image: 'update-profile',
                        route: EditProfilePage.tag),
                    _buildListTile(
                        context: context,
                        title: 'Change_password',
                        image: 'change-Password',
                        route: ChangePasswordPage.tag),
                    _buildListTile(
                        context: context,
                        title: 'About_us',
                        image: 'info',
                        route: AboutUsPage.tag),
                    _buildListTile(
                        context: context,
                        title: 'Ratings',
                        image: 'rating',
                        route: RatingsPage.tag),
                  
                    _buildListTile(
                        context: context,
                        title: 'Settings',
                        image: 'settings',
                        route: SettingsPage.tag),
                    _buildListTile(
                        context: context,
                        title: 'contact_us',
                        image: 'support',
                        route: ContactUsPage.tag),
                    ListTile(
                      leading: CustomIcon(
                        radius: 25,
                        image: 'logout',
                        imageSize: 25,
                      ),
                      title: MyText.mText(
                          context: context, text: 'exit', color: Colors.red),
                      onTap: () {
                        MyPopup.showPopUp(
                          context: context,
                          title: appLocalizations.getMessageByLangAndKey(appLanguageCode, 'confirmation'),
                                      message: appLocalizations.getMessageByLangAndKey(appLanguageCode, 'are_you_sure_exit'),
                          function: () {
                            savePrefs('loginModel', null).then((onValue) {});
                            Navigator.of(context).pushAndRemoveUntil(
                                MaterialPageRoute(
                                  builder: (context) => SplashScreenPage(),
                                ),
                                (Route<dynamic> route) => false);
                          },
                        );
                      },
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildListTile(
      {BuildContext context, String title, String image, String route}) {
    return ListTile(
      leading: CustomIcon(
        radius: 25,
        image: image,
        imageSize: 25,
      ),
      title: MyText.mText(
        context: context,
        text: title,
      ),
      trailing: Icon(
        Icons.arrow_forward_ios,
        size: 20,
      ),
      onTap: () {
        Navigator.pushNamed(context, route);
      },
    );
  }
}
