import 'package:flutter/material.dart';
import 'package:lookinservice/PreDefined/app_constants.dart';
import 'package:lookinservice/PreDefined/localization.dart';
import 'package:lookinservice/Utils/size_config.dart';
import 'package:lookinservice/Widgets/text_widgets.dart';

class MorePage extends StatefulWidget {
    static const tag = '/MorePage';

  @override
  _MorePageState createState() => _MorePageState();
}

class _MorePageState extends State<MorePage> {
  AppLocalizations appLocalizations;

  @override
  Widget build(BuildContext context) {
    SizeConfig.init(context);
    appLocalizations = AppLocalizations.of(context);
    return Directionality(
      textDirection: isRightToLeft ? TextDirection.rtl : TextDirection.ltr,
      child: Scaffold(
        appBar: AppBar(
            title: MyText.mTextBold(
              context: context,
          text: 'Welcome',
        )),
        body: Container(),
      ),
    );
  }
}
