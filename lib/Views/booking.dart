import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:lazy_load_scrollview/lazy_load_scrollview.dart';
import 'package:lookinservice/Generics/NetworkUtil.dart';
import 'package:lookinservice/Models/api_response_model.dart';
import 'package:lookinservice/Models/booking_model.dart';
import 'package:lookinservice/PreDefined/app_constants.dart';
import 'package:lookinservice/PreDefined/localization.dart';
import 'package:lookinservice/Utils/size_config.dart';
import 'package:lookinservice/Views/Messages/chat_page.dart';
import 'package:lookinservice/Widgets/button.dart';
import 'package:lookinservice/Widgets/custom_appbar_widget.dart';
import 'package:lookinservice/Widgets/no_data_widget.dart';
import 'package:lookinservice/Widgets/text_widgets.dart';
import 'package:lookinservice/Widgets/user_photo.dart';

class BookingsPage extends StatefulWidget {
  @override
  _BookingsPageState createState() => _BookingsPageState();
}

class _BookingsPageState extends State<BookingsPage>
    with TickerProviderStateMixin {
  AppLocalizations appLocalizations;
  TabController _tabController;
  bool _isLoadingAccepted = false;
  bool _isLoadingRejected = false;
  bool _isLoadingAttented = false;
  bool _isLoadingNotAttented = false;

  final RoundedLoadingButtonController _btnController =
      RoundedLoadingButtonController();
  TextEditingController _noticeController = TextEditingController();
  String _reportNote;
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  List<BookingDataModel> _acceptedBookingList = [];
  List<BookingDataModel> _rejectedBookingList = [];
  List<BookingDataModel> _attendedBookingList = [];
  List<BookingDataModel> _notAttendedBookingList = [];
  String requestStatus = '';
  bool ratingDone = false;
  bool cancelDone = false;
  int _pageAccepted = 1;
  int _lastPageAccepted = 1;

  int _pageRejected = 1;
  int _lastPageRejected = 1;

  int _pageAttended = 1;
  int _lastPageAttended = 1;

  int _pageNotAttended = 1;
  int _lastPageNotAttended = 1;
  var _context;
  @override
  void initState() {
    super.initState();
    _noticeController.text = '';
    _getAcceptedBookings();
    _getRejectedBookings();
    _getAttendedBookings();
    _getNotAttendedBookings();
    _tabController = TabController(vsync: this, length: 4);
    _tabController.addListener(_handleTabSelection);
  }

  @override
  void dispose() {
    _tabController.dispose();
    _noticeController.dispose();
    super.dispose();
  }

  void _handleTabSelection() {
    if (_tabController.indexIsChanging) {
      switch (_tabController.index) {
        case 0:
          print('Page 1 tapped.');
          break;
        case 1:
          print('Page 2 tapped.');
          break;
        case 2:
          print('Page 3 tapped.');
          break;
        case 3:
          print('Page 4 tapped.');
          break;
      }
    }
  }

  Future<void> _refreshAcceptedData() async {
    print('refreshing data');
     if (_pageAccepted ==1){
   _acceptedBookingList.clear();

 }
    setState(() {
      _getAcceptedBookings();
    });
  }

  Future<void> _refreshRejectedData() async {
       if (_pageRejected ==1){
   _rejectedBookingList.clear();

 }
    setState(() {
      _getRejectedBookings();
    });
  }

  Future<void> _refreshAttendedData() async {
       if (_pageAttended ==1){
   _attendedBookingList.clear();

 }
    setState(() {
      _getAttendedBookings();
    });
  }

  Future<void> _refreshNotAttendedData() async {
       if (_pageNotAttended ==1){
   _notAttendedBookingList.clear();

 }
    setState(() {
      _getNotAttendedBookings();
    });
  }

  void setState(fn) {
    if (mounted) {
      super.setState(fn);
    }
  }

  @override
  Widget build(BuildContext context) {
    appLocalizations = AppLocalizations.of(context);
    SizeConfig.init(context);
    return Directionality(
      textDirection: isRightToLeft ? TextDirection.rtl : TextDirection.ltr,
      child: Scaffold(
        key: _scaffoldKey,
        appBar: CustomAppBar.appBar(
          context,
          'Bookings',
          TabBar(
            isScrollable: true,
            unselectedLabelColor: Colors.grey,
            indicatorColor: primaryColor,
            labelColor: primaryColor,
            indicatorWeight: 3.0,
            tabs: <Widget>[
              Tab(
                child: MyText.sText(
                  context: context,
                  text: 'The_Confirmed',
                ),
              ),
              Tab(
                child: MyText.sText(
                  context: context,
                  text: 'The_Rejected',
                ),
              ),
              Tab(
                child: MyText.sText(
                  context: context,
                  text: 'Client_attended',
                ),
              ),
              Tab(
                child: MyText.sText(
                  context: context,
                  text: 'Client_did_not_attend',
                ),
              ),
            ],
            controller: _tabController,
          ),
        ),
        body: TabBarView(
          physics: NeverScrollableScrollPhysics(),
          controller: _tabController,
          children: <Widget>[
            constLoginModel.id == 0
                ? NoDataAvailable(
                    image: 'assets/reservation-empty.png',
                    text: 'Sorry_no_bookings_available',
                    visible: false,
                  )
                : _isLoadingAccepted
                    ? Center(child: CircularProgressIndicator())
                    : _acceptedBookingList.length == 0
                        ? NoDataAvailable(
                            image: 'assets/reservation-empty.png',
                            text: 'Sorry_no_bookings_available',
                            visible: true,
                            onPressed: _refreshAcceptedData,
                          )
                        : LazyLoadScrollView(
                            scrollOffset: 100,
                            onEndOfPage: _refreshAcceptedData,
                            child: RefreshIndicator(
                              onRefresh: _refreshAcceptedData,
                              child: ListView.builder(
                                scrollDirection: Axis.vertical,
                                physics: ScrollPhysics(),
                                shrinkWrap: true,
                                itemCount: _acceptedBookingList.length,
                                itemBuilder: (context, index) {
                                  return Column(
                                    children: <Widget>[
                                      _buildItems(index, _acceptedBookingList,
                                          _acceptedBottom(index), true, false),
                                    ],
                                  );
                                },
                              ),
                            ),
                          ),
            constLoginModel.id == 0
                ? NoDataAvailable(
                    image: 'assets/reservation-empty.png',
                    text: 'Sorry_no_bookings_available',
                    visible: false,
                  )
                : _isLoadingRejected
                    ? Center(child: CircularProgressIndicator())
                    : _rejectedBookingList.length == 0
                        ? NoDataAvailable(
                            image: 'assets/reservation-empty.png',
                            text: 'Sorry_no_bookings_available',
                            visible: true,
                            onPressed: _refreshRejectedData,
                          )
                        : LazyLoadScrollView(
                            scrollOffset: 100,
                            onEndOfPage: _refreshRejectedData,
                            child: RefreshIndicator(
                              onRefresh: _refreshRejectedData,
                              child: ListView.builder(
                                scrollDirection: Axis.vertical,
                                physics: ScrollPhysics(),
                                shrinkWrap: true,
                                itemCount: _rejectedBookingList.length,
                                itemBuilder: (context, index) {
                                  return Column(
                                    children: <Widget>[
                                      _buildItems(index, _rejectedBookingList,
                                          _rejectedBottom(index), false, false),
                                    ],
                                  );
                                },
                              ),
                            ),
                          ),
            constLoginModel.id == 0
                ? NoDataAvailable(
                    image: 'assets/reservation-empty.png',
                    text: 'Sorry_no_bookings_available',
                    visible: false,
                  )
                : _isLoadingAttented
                    ? Center(child: CircularProgressIndicator())
                    : _attendedBookingList.length == 0
                        ? NoDataAvailable(
                            image: 'assets/reservation-empty.png',
                            text: 'Sorry_no_bookings_available',
                            visible: true,
                            onPressed: _refreshAttendedData,
                          )
                        : LazyLoadScrollView(
                            scrollOffset: 100,
                            onEndOfPage: _refreshAttendedData,
                            child: RefreshIndicator(
                              onRefresh: _refreshAttendedData,
                              child: ListView.builder(
                                scrollDirection: Axis.vertical,
                                physics: ScrollPhysics(),
                                shrinkWrap: true,
                                itemCount: _attendedBookingList.length,
                                itemBuilder: (context, index) {
                                  return _buildItems(
                                      index,
                                      _attendedBookingList,
                                      Container(),
                                      false,
                                      false);
                                },
                              ),
                            ),
                          ),
            constLoginModel.id == 0
                ? NoDataAvailable(
                    image: 'assets/reservation-empty.png',
                    text: 'Sorry_no_bookings_available',
                    visible: false,
                  )
                : _isLoadingNotAttented
                    ? Center(child: CircularProgressIndicator())
                    : _notAttendedBookingList.length == 0
                        ? NoDataAvailable(
                            image: 'assets/reservation-empty.png',
                            text: 'Sorry_no_bookings_available',
                            visible: true,
                            onPressed: _refreshNotAttendedData,
                          )
                        : LazyLoadScrollView(
                            scrollOffset: 100,
                            onEndOfPage: _refreshNotAttendedData,
                            child: RefreshIndicator(
                              onRefresh: _refreshNotAttendedData,
                              child: ListView.builder(
                                scrollDirection: Axis.vertical,
                                physics: ScrollPhysics(),
                                shrinkWrap: true,
                                itemCount: _notAttendedBookingList.length,
                                itemBuilder: (context, index) {
                                  return _buildItems(
                                      index,
                                      _notAttendedBookingList,
                                      Container(),
                                      false,
                                      true);
                                },
                              ),
                            ),
                          ),
          ],
        ),
      ),
    );
  }

  Widget _buildItems(int index, List<BookingDataModel> list, Widget bottom,
      bool messageVisible, bool blockVisible) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Container(
          margin: EdgeInsets.only(
              top: hBlock * 2, left: hBlock * 2, right: hBlock * 2),
          // margin: EdgeInsets.only(top: 8, left: 8, right: 8),
          // height: vBlock * 30,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10.0),
            border: Border.all(
              color: accentColor,
              width: 1,
            ),
          ),
          child: Column(
            children: <Widget>[
              Row(
                children: <Widget>[
                  Container(
                    width: hBlock * 30,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        UserPhoto(
                          height: hBlock * 25,
                          imageUrl: list[index].user.image,
                        ),
                        Stack(
                          children: <Widget>[
                            Visibility(
                              visible: messageVisible,
                              child: InkWell(
                                onTap: () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => ChatPage(
                                        title: list[index].user.name,
                                        id: list[index].id,
                                        visible: true,
                                      ),
                                    ),
                                  );
                                },
                                child: CircleAvatar(
                                    backgroundImage:
                                        AssetImage('assets/send-message.png')),
                              ),
                            ),
                            Visibility(
                              visible: blockVisible,
                              child: Positioned(
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: InkWell(
                                    onTap: () {
                                      _showReportPopUp(context, index);
                                    },
                                    child: CircleAvatar(
                                      radius: 20,
                                      backgroundColor: Colors.red,
                                      child: CircleAvatar(
                                          backgroundColor: Colors.red,
                                          radius: 15,
                                          child: Image.asset(
                                            'assets/ban.png',
                                            fit: BoxFit.contain,
                                          )),
                                    ),
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(top: 8, left: 8, right: 8),
                    width: hBlock * 65,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 5),
                          child: MyText.sTextNLBold(
                              context: context, text: list[index].user.name),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 5),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              MyText.sText(
                                  context: context, text: 'Booking_Date'),
                              MyText.sTextNL(
                                  context: context, text: list[index].date),
                              // Text(list[index].date),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 5),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              MyText.sText(context: context, text: 'Time'),
                              MyText.sTextNL(
                                  context: context, text: list[index].time),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 5),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              MyText.sText(
                                  context: context, text: 'No_Persons'),
                              MyText.sTextNL(
                                context: context,
                                text: list[index].slotNo.toString(),
                              ),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 5),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              MyText.sText(
                                  context: context, text: 'Booking_notice'),
                              Spacer(),
                              Flexible(
                                child: MyText.sTextNL(
                                    context: context,
                                    text: list[index].note == null
                                        ? ''
                                        : list[index].note),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              bottom
            ],
          ),
        ),
      ],
    );
  }

  Widget _acceptedBottom(int index) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[
        _button(
            context: context,
            color: Colors.green,
            title: 'Client_attended',
            onPressed: () {
              setState(() {
                _postChangeStatusClientAttended(index);
              });
            }),
        _button(
            context: context,
            color: Colors.red,
            title: 'Client_did_not_attend',
            onPressed: () {
              setState(() {
                _postChangeStatusClientNotAttended(index);
              });
            }),
      ],
    );
  }

  Widget _rejectedBottom(int index) {
    return Row(
      children: <Widget>[
        Spacer(),
        Container(
          width: hBlock * 65,
          padding: const EdgeInsets.all(8.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              MyText.sText(
                  context: context,
                  text: 'cause_of_rejection',
                  color: Colors.red),
              Flexible(
                child: MyText.sTextNL(
                    context: context,
                    text: _rejectedBookingList[index].reason.reason,
                    color: Colors.red),
              )
            ],
          ),
        ),
      ],
    );
  }

  Widget _button(
      {BuildContext context, String title, Color color, Function onPressed}) {
    return Container(
      margin: EdgeInsets.all(hBlock * 2),
      width: hBlock * 40,
      height: 50,
      child: RaisedButton(
          color: color,
          elevation: 2,
          onPressed: onPressed,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10),
          ),
          child: FittedBox(
            child: MyText.sTextBold(
              context: context,
              text: title,
              color: Colors.white,
            ),
          )),
    );
  }

  _showReportPopUp(BuildContext context, int index) {
    AppLocalizations appLocalizations = AppLocalizations.of(context);
    showDialog(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return StatefulBuilder(
          builder: (BuildContext context, StateSetter setState) {
            return Directionality(
              textDirection:
                  isRightToLeft ? TextDirection.rtl : TextDirection.ltr,
              child: SimpleDialog(
                contentPadding: MediaQuery.of(context).viewInsets +
                    const EdgeInsets.symmetric(
                        horizontal: 20.0, vertical: 20.0),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10)),
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      SizedBox(
                        width: 30,
                      ),
                      MyText.mTextBold(context: context, text: 'Report_user'),
                      IconButton(
                        iconSize: 30,
                        icon: Icon(Icons.close),
                        onPressed: () {
                          Navigator.of(context).pop();
                          _noticeController.clear();
                        },
                      ),
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: TextFormField(
                      onChanged: (value) {
                        setState(() {
                          _reportNote = value;
                        });
                      },
                      controller: _noticeController,
                      textInputAction: TextInputAction.newline,
                      keyboardType: TextInputType.multiline,
                      maxLines: 4,
                      decoration: InputDecoration(
                        labelText: appLocalizations.getMessageByLangAndKey(
                            appLanguageCode, 'Write_your_notes'),
                        hintText: appLocalizations.getMessageByLangAndKey(
                            appLanguageCode, 'Write_your_notes'),
                        hintStyle: TextStyle(
                          fontSize: hBlock * 5,
                        ),
                        labelStyle: TextStyle(
                          fontSize: hBlock * 5,
                        ),
                      ),
                    ),
                  ),
                  IgnorePointer(
                    ignoring: _noticeController.text.isEmpty,
                    child: Container(
                      // margin: EdgeInsets.all(8),
                      width: hBlock * 85,
                      height: 60,
                      child: RoundedLoadingButton(
                        color: _noticeController.text.isEmpty
                            ? accentColor
                            : Colors.red,
                        width: hBlock * 85,
                        controller: _btnController,
                        onPressed: () {
                          setState(() {
                            _reportUser(index);
                          });
                        },
                        child: MyText.mText(
                            context: context,
                            text: 'Block_user',
                            color: Colors.white),
                      ),
                    ),
                  ),
                ],
              ),
            );
          },
        );
      },
    );
  }

  _getAcceptedBookings() async {
    if (constLoginModel == null) {
      return;
    }
    _isLoadingAccepted = true;
    ApiResponseModel responseModel = await NetworkUtil.get(
      'bookings/2?page=$_pageAccepted',
    );
    if (responseModel == null) {
      return;
    }
    if (responseModel.statuscode == 200) {
      BookingModel model = BookingModel.fromJson(responseModel.data);
      if (model == null) {
        return;
      }
      print(responseModel.data);
      _lastPageAccepted = model.lastPage;

      if (model.nextPageUrl != null) {
        _pageAccepted++;
      }
      _acceptedBookingList.addAll(model.data);
      _acceptedBookingList.forEach((f) {
        print(f.id.toString());
      });

      if (mounted) {
        setState(() {
          _isLoadingAccepted = false;
        });
      }
    } else {
      _isLoadingAccepted = false;
    }
  }

  _getRejectedBookings() async {
    if (constLoginModel == null) {
      return;
    }
    _isLoadingRejected = true;
    ApiResponseModel responseModel = await NetworkUtil.get(
      'bookings/6?page=$_pageRejected',
    );
    if (responseModel == null) {
      return;
    }
    if (responseModel.statuscode == 200) {
      BookingModel model = BookingModel.fromJson(responseModel.data);
      if (model == null) {
        return;
      }
      print(model.data);
      _lastPageRejected = model.lastPage;

      if (model.nextPageUrl != null) {
        _pageRejected++;
      }

      _rejectedBookingList.addAll(model.data);

      if (mounted) {
        setState(() {
          _isLoadingRejected = false;
        });
      }
    } else {
      _isLoadingRejected = false;
    }
  }

  _getAttendedBookings() async {
    if (constLoginModel == null) {
      return;
    }
    _isLoadingAttented = true;
    ApiResponseModel responseModel = await NetworkUtil.get(
      'bookings/3?page=$_pageAttended',
    );
    if (responseModel == null) {
      return;
    }
    if (responseModel.statuscode == 200) {
      BookingModel model = BookingModel.fromJson(responseModel.data);
      if (model == null) {
        return;
      }
      print(responseModel.statuscode);

      _lastPageAttended = model.lastPage;
      if (model.nextPageUrl != null) {
        _pageAttended++;
      }

      _attendedBookingList.addAll(model.data);

      if (mounted) {
        setState(() {
          _isLoadingAttented = false;
        });
      }
    } else {
      _isLoadingAttented = false;
    }
  }

  _getNotAttendedBookings() async {
    if (constLoginModel == null) {
      return;
    }
    _isLoadingNotAttented = true;
    ApiResponseModel responseModel = await NetworkUtil.get(
      'bookings/4?page=$_pageNotAttended',
    );
    if (responseModel == null) {
      return;
    }
    if (responseModel.statuscode == 200) {
      BookingModel model = BookingModel.fromJson(responseModel.data);
      if (model == null) {
        return;
      }
      print(model.data);
      _lastPageNotAttended = model.lastPage;
      if (model.nextPageUrl != null) {
        _pageNotAttended++;
      }

      _notAttendedBookingList.addAll(model.data);

      if (mounted) {
        setState(() {
          _isLoadingNotAttented = false;
        });
      }
    } else {
      _isLoadingNotAttented = false;
    }
  }

  _postChangeStatusClientAttended(int index) async {
    Map<String, dynamic> jsonMap = {
      "status": 3,
      "booking_id": _acceptedBookingList[index].id,
    };
    String jsonString = json.encode(jsonMap);
    ApiResponseModel responseModel = await NetworkUtil.post(
      'bookings',
      jsonString,
    );
    if (responseModel == null) {
      return;
    }
    if (responseModel.statuscode == 200) {
      setState(() {
        _acceptedBookingList.removeAt(index);
      });
      Scaffold.of(context).showSnackBar(SnackBar(
          backgroundColor: primaryColor,
          content: Text(
            appLocalizations.getMessageByLangAndKey(
                appLanguageCode, 'Client_attended'),
            textAlign: TextAlign.center,
          )));
    } else {
      return;
    }
  }

  _postChangeStatusClientNotAttended(int index) async {
    Map<String, dynamic> jsonMap = {
      "status": 4,
      "booking_id": _acceptedBookingList[index].id,
    };
    String jsonString = json.encode(jsonMap);
    ApiResponseModel responseModel = await NetworkUtil.post(
      'bookings',
      jsonString,
    );
    print(responseModel.data);
    if (responseModel == null) {
      return;
    }
    if (responseModel.statuscode == 200) {
      setState(() {
        _acceptedBookingList.removeAt(index);
      });
      Scaffold.of(context).showSnackBar(SnackBar(
          duration: Duration(seconds: 2),
          backgroundColor: Colors.red,
          content: Text(
            appLocalizations.getMessageByLangAndKey(
                appLanguageCode, 'Client_did_not_attend'),
            textAlign: TextAlign.center,
          )));
    } else {
      return;
    }
  }

  _reportUser(int index) async {
    Map<String, dynamic> jsonMap = {"msg": _noticeController.text};
    String jsonString = json.encode(jsonMap);
    ApiResponseModel responseModel = await NetworkUtil.put(
      'bookings/${_notAttendedBookingList[index].id}',
      jsonString,
    );
    if (responseModel == null) {
      return;
    }
    if (responseModel.statuscode == 200) {
      _btnController.success();
      print(responseModel.message);
      Navigator.of(context).pop();
      setState(() {
        _noticeController.clear();
      });
      Scaffold.of(context).showSnackBar(SnackBar(
          duration: Duration(seconds: 2),
          backgroundColor: Colors.red,
          content: Text(
            appLocalizations.getMessageByLangAndKey(
                appLanguageCode, 'Block_user'),
            textAlign: TextAlign.center,
          )));
    } else {
      _btnController.reset();
      return;
    }
  }
}
