import 'package:flutter/material.dart';
import 'package:lookinservice/Models/login_model.dart';
import 'package:lookinservice/Views/Auth/login_page.dart';
import '../../Generics/app_prefs.dart';
import '../../PreDefined/app_constants.dart';
import '../../PreDefined/localization.dart';
import '../../Utils/size_config.dart';

class LanguagePage extends StatefulWidget {
  static const tag = '/languagePage';

  @override
  _LanguagePageState createState() => _LanguagePageState();
}

class _LanguagePageState extends State<LanguagePage> {

  AppLocalizations appLocalizations;
  Color _enbuttonColor = primaryColor;
  Color _arbuttonColor = accentColor;

  @override
  void initState() {
    constLoginModel = LoginModel();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig.init(context);
    appLocalizations = AppLocalizations.of(context);
    return Scaffold(
      backgroundColor: Colors.white,
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
            height: 100,
            child: Center(
              child: Image.asset(
                'assets/logo1.png',
                fit: BoxFit.contain,
              ),
            ),
          ),
          Column(
            children: <Widget>[
              Text( 'Select App Language',style: TextStyle(fontSize:26)),
              Text( 'اختر لغة التطبيق',style: TextStyle(fontSize:26)),
              Padding(
                padding: const EdgeInsets.all(40.0),
                child: Center(
                  child: Image.asset(
                    'assets/language.png',
                    fit: BoxFit.contain,
                  ),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  RaisedButton(
                    color: _enbuttonColor,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(
                        5.0,
                      ),
                    ),
                    child: Text(
                      'English',style: TextStyle(fontSize:20)
                    ),
                    onPressed: () {
                      setState(() {
                        appLanguageId = 1;
                        appLanguageCode = 'en';
                        isRightToLeft = false;
                        savePrefsInt('selected_language', 1);
                        _arbuttonColor = accentColor;
                        _enbuttonColor = primaryColor;
                      });
                       Navigator.pushReplacementNamed(context, LoginPage.tag);
                    
                    },
                  ),
                  SizedBox(
                    width: 50,
                  ),
                  RaisedButton(
                    color: _arbuttonColor,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(
                        5.0,
                      ),
                    ),
                    child: Text(
                      'العربية',style: TextStyle(fontSize:20)
                    ),
                    onPressed: () {
                      setState(() {
                        appLanguageId = 2;
                        appLanguageCode = 'ar';
                        isRightToLeft = true;
                        savePrefsInt('selected_language', 2);
                        _arbuttonColor = primaryColor;
                        _enbuttonColor = accentColor;
                      });
                       Navigator.pushReplacementNamed(context, LoginPage.tag);
                    
                    },
                  ),
                ],
              ),
              SizedBox(
                height: 100,
              ),
            ],
          ),
        ],
      ),
    );
  }
}
