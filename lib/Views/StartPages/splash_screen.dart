import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:lookinservice/Generics/app_prefs.dart';
import 'package:lookinservice/Models/login_model.dart';
import 'package:lookinservice/PreDefined/app_constants.dart';
import 'package:lookinservice/Views/Auth/login_page.dart';
import 'package:lookinservice/Widgets/dashboard.dart';

import '../../PreDefined/app_constants.dart';
import '../../PreDefined/localization.dart';
import '../../Views/StartPages/language_page.dart';

class SplashScreenPage extends StatefulWidget {
  static const tag = '/SplashScreenPage';
  @override
  _SplashScreenPageState createState() => _SplashScreenPageState();
}

class _SplashScreenPageState extends State<SplashScreenPage> {
  AppLocalizations appLocalizations;
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  _readDataFromSharedPref() async {
    await Future.delayed(Duration(seconds: 2));
//  Navigator.pushReplacementNamed(
//           _scaffoldKey.currentContext, LanguagePage.tag);
    var _selectedLanguage = await getpreferencesInt('selected_language');
    if (_selectedLanguage == null) {
      appLanguageId = 1;
      isRightToLeft = false;
      Navigator.pushReplacementNamed(
          _scaffoldKey.currentContext, LanguagePage.tag);
      return;
    } else {
      appLanguageId = _selectedLanguage;
      if (appLanguageId == 1) {
        appLanguageCode = 'en';
        isRightToLeft = false;
      } else {
        appLanguageCode = 'ar';
        isRightToLeft = true;
      }
      Navigator.pushReplacementNamed(
          _scaffoldKey.currentContext, LoginPage.tag);
    }

    var userLoggedModel = await getpreferences('loginModel');
    if (userLoggedModel == null) {
      //userLoggedIn = false;
      Navigator.pushReplacementNamed(
          _scaffoldKey.currentContext, LoginPage.tag);
    } else {
      constLoginModel = LoginModel.fromJson(json.decode(userLoggedModel));

      // userLoggedIn=true;
      // apiToken = constUserLoginModel.rememberToken;
      print(constLoginModel.rememberToken);
      Navigator.pushReplacementNamed(
          _scaffoldKey.currentContext, Dashboard.tag);
      return;
    }
  }

  @override
  void initState() {
    constLoginModel = LoginModel();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    _readDataFromSharedPref();
    appLocalizations = AppLocalizations.of(context);
    return Directionality(
      textDirection: isRightToLeft ? TextDirection.rtl : TextDirection.ltr,
      child: Scaffold(
        key: _scaffoldKey,
        backgroundColor: Colors.white,
        body: Stack(
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                image: DecorationImage(
                  fit: BoxFit.fill,
                  image: AssetImage('assets/bg.png'),
                ),
              ),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Center(
                  child: Image.asset(
                    'assets/logo.png',
                    fit: BoxFit.contain,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
