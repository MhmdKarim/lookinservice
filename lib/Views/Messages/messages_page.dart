import 'package:flutter/material.dart';
import 'package:lazy_load_scrollview/lazy_load_scrollview.dart';
import 'package:lookinservice/Generics/NetworkUtil.dart';
import 'package:lookinservice/Generics/generic_functions.dart';
import 'package:lookinservice/Models/all_inbox_model.dart';
import 'package:lookinservice/Models/api_response_model.dart';
import 'package:lookinservice/Views/Messages/chat_page.dart';
import 'package:lookinservice/Widgets/custom_appbar_widget.dart';
import 'package:lookinservice/Widgets/no_data_widget.dart';
import 'package:lookinservice/Widgets/text_widgets.dart';
import 'package:lookinservice/Widgets/user_photo.dart';
import '../../PreDefined/app_constants.dart';
import '../../PreDefined/localization.dart';
import '../../Utils/size_config.dart';

class MessagesPage extends StatefulWidget {
  static const tag = '/MessagesPage';

  @override
  _MessagesPageState createState() => _MessagesPageState();
}

class _MessagesPageState extends State<MessagesPage>
    with TickerProviderStateMixin {
  AppLocalizations appLocalizations;
  bool _isLoading = true;
  TabController _tabController;
  List<MessagesDataModel> _openMessagesList = [];
  List<MessagesDataModel> _closedMessagesList = [];

  int _pageOpened = 1;
  int _lastPageOpened = 1;

  int _pageClosed = 1;
  int _lastPageClosed = 1;

  @override
  void initState() {
    _getOpenMessages();
    _getClosedMessages();
    _tabController = TabController(vsync: this, length: 2);
    _tabController.addListener(_handleTabSelection);
    super.initState();
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  void _handleTabSelection() {
    if (_tabController.indexIsChanging) {
      switch (_tabController.index) {
        case 0:
          print('Page 1 tapped.');
          break;
        case 1:
          print('Page 2 tapped.');
          break;
      }
    }
  }

  Future<void> _refreshOpenMessagesData() async {
         if (_pageOpened ==1){
   _openMessagesList.clear();

 }
    setState(() {
      _getOpenMessages();
    });
  }

  Future<void> _refreshClosedMessagesData() async {
     if (_pageClosed ==1){
   _closedMessagesList.clear();

 }
    setState(() {
      _getClosedMessages();
    });
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig.init(context);
    appLocalizations = AppLocalizations.of(context);
    return Directionality(
      textDirection: isRightToLeft ? TextDirection.rtl : TextDirection.ltr,
      child: Scaffold(
        appBar: CustomAppBar.appBar(
          context,
          'messages',
          TabBar(
            isScrollable: true,
            unselectedLabelColor: Colors.grey,
            indicatorColor: primaryColor,
            labelColor: primaryColor,
            indicatorWeight: 3.0,
            tabs: <Widget>[
              Tab(
                child: MyText.sText(
                  context: context,
                  text: 'Open_messages',
                ),
              ),
              Tab(
                child: MyText.sText(
                  context: context,
                  text: 'Closed_messages',
                ),
              ),
            ],
            controller: _tabController,
          ),
        ),
        body: TabBarView(
          physics: NeverScrollableScrollPhysics(),
          controller: _tabController,
          children: <Widget>[
            _isLoading
                ? Center(child: CircularProgressIndicator())
                : _openMessagesList.length == 0
                    ? NoDataAvailable(
                        image: 'assets/messages-empty.png',
                        text: 'Sorry_no_messages_available',
                        visible: true,
                        onPressed: _refreshOpenMessagesData,
                      )
                    : RefreshIndicator(
                        onRefresh: _refreshOpenMessagesData,
                        child: ListView(
                          children: <Widget>[
                            LazyLoadScrollView(
                              scrollOffset: 0,
                              onEndOfPage: _refreshOpenMessagesData,
                              child: ListView.builder(
                                scrollDirection: Axis.vertical,
                                physics: ScrollPhysics(),
                                shrinkWrap: true,
                                itemCount: _openMessagesList.length,
                                itemBuilder: (context, index) {
                                  return _buildItems(
                                      index, _openMessagesList, true);
                                },
                              ),
                            ),
                          ],
                        ),
                      ),
            _isLoading
                ? Center(child: CircularProgressIndicator())
                : _closedMessagesList.length == 0
                    ? NoDataAvailable(
                        image: 'assets/messages-empty.png',
                        text: 'Sorry_no_messages_available',
                        visible: true,
                        onPressed: _refreshClosedMessagesData,
                      )
                    : LazyLoadScrollView(
                        scrollOffset: 0,
                        onEndOfPage: _refreshOpenMessagesData,
                        child: RefreshIndicator(
                          onRefresh: _refreshClosedMessagesData,
                          child: ListView(
                            children: <Widget>[
                              ListView.builder(
                                scrollDirection: Axis.vertical,
                                physics: ScrollPhysics(),
                                shrinkWrap: true,
                                itemCount: _closedMessagesList.length,
                                itemBuilder: (context, index) {
                                  return _buildItems(
                                      index, _closedMessagesList, false);
                                },
                              ),
                            ],
                          ),
                        ),
                      ),
          ],
        ),
      ),
    );
  }

  Widget _buildItems(int index, List<MessagesDataModel> list, bool visible) {
    return InkWell(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => ChatPage(
              title: list[index].user.name,
              id: list[index].id,
              visible: visible,
            ),
          ),
        );
      },
      child: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              height: 1,
              color: Colors.transparent,
            ),
            Row(
              children: <Widget>[
                UserPhoto(
                  height: hBlock * 15,
                  imageUrl: list[index].user.image,
                ),
                Container(
                  width: hBlock * 75,
                  padding: EdgeInsets.all(hBlock * 2),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          MyText.sTextNLBold(
                              context: context, text: list[index].user.name),
                          Text(GenericFunctions.intToDate(
                              list[index].lastInbox.date))
                        ],
                      ),
                      Text(
                        list[index].lastInbox.msg,
                        maxLines: 2,
                      )
                    ],
                  ),
                ),
              ],
            ),
            Container(
              height: 1,
              color: accentColor,
            )
          ],
        ),
      ),
    );
  }

  _getClosedMessages() async {
    ApiResponseModel responseModel = await NetworkUtil.get(
      'inboxClose?page=$_pageClosed',
    );
    if (responseModel == null) {
      return;
    }
    if (responseModel.statuscode == 200) {
      MessagesModel model = MessagesModel.fromJson(responseModel.data);
      if (model == null) {
        return;
      }
      _lastPageClosed = model.lastPage;
      if (model.nextPageUrl != null) {
        _pageClosed++;
      }

      _closedMessagesList.addAll(model.data);
      if (mounted) {
        setState(() {
          _isLoading = false;
        });
      }
    } else {
      _isLoading = true;
    }
  }

  _getOpenMessages() async {
    ApiResponseModel responseModel = await NetworkUtil.get(
      'inbox?page=$_pageOpened',
    );
    if (responseModel == null) {
      return;
    }
    if (responseModel.statuscode == 200) {
      MessagesModel model = MessagesModel.fromJson(responseModel.data);
      if (model == null) {
        return;
      }

      _lastPageOpened = model.lastPage;
      if (model.nextPageUrl != null) {
        _pageOpened++;
      }

      _openMessagesList.addAll(model.data);
      if (mounted) {
        setState(() {
          _isLoading = false;
        });
      }
    } else {
      _isLoading = true;
    }
  }
}
