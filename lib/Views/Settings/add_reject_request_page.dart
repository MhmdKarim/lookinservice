import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:lookinservice/Generics/NetworkUtil.dart';
import 'package:lookinservice/Models/api_response_model.dart';
import 'package:lookinservice/PreDefined/app_constants.dart';
import 'package:lookinservice/PreDefined/localization.dart';
import 'package:lookinservice/Utils/size_config.dart';
import 'package:lookinservice/Widgets/button.dart';
import 'package:lookinservice/Widgets/custom_appbar_widget.dart';
import 'package:lookinservice/Widgets/text_widgets.dart';

class AddRejectRequestReasons extends StatefulWidget {
  static const tag = '/AddRejectRequestReasons';

  @override
  _AddRejectRequestReasonsState createState() =>
      _AddRejectRequestReasonsState();
}

class _AddRejectRequestReasonsState extends State<AddRejectRequestReasons> {
  AppLocalizations appLocalizations;
  TextEditingController _reasonController = TextEditingController();
  List<String> _reasons = [];
  final _formKey = GlobalKey<FormState>();
  final RoundedLoadingButtonController _btnController =
      new RoundedLoadingButtonController();
  var _scaffoldKey = GlobalKey<ScaffoldState>();

@override
  void dispose() {
_reasonController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig.init(context);
    appLocalizations = AppLocalizations.of(context);
    return Directionality(
      textDirection: isRightToLeft ? TextDirection.rtl : TextDirection.ltr,
      child: Scaffold(
        key: _scaffoldKey,
        appBar: CustomAppBar.appBar(context, 'Reasons_of_rejection', null),
        body: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.all(hBlock * 2),
                child: MyText.sText(
                    context: context, text: 'This_menu_of_reasons___'),
              ),
              Form(
                key: _formKey,
                child: Row(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.all(hBlock * 2),
                      width: hBlock * 85,
                      child: TextFormField(
                        controller: _reasonController,
                        cursorColor: primaryColor,
                        validator: (value) {
                          if (value.isEmpty) {
                            return appLocalizations.getMessageByLangAndKey(
                                appLanguageCode, 'Write_the_reason');
                          }
                          return null;
                        },
                      ),
                    ),
                    IconButton(
                      iconSize: hBlock * 10,
                      icon: Icon(Icons.add),
                      color: primaryColor,
                      onPressed: () {
                        if (!_formKey.currentState.validate()) {
                          return;
                        }
                        setState(() {
                          if (_reasons.length > 0) {
                            _reasons.removeAt(0);
                            _reasons.add(_reasonController.text);
                            _reasonController.clear();
                            print(_reasons);
                          }else{
                              _reasons.add(_reasonController.text);
                            _reasonController.clear();
                          }
                        });
                      },
                    )
                  ],
                ),
              ),
              Container(
                height: vBlock * 50,
                child: ListView.separated(
                  separatorBuilder: (context, index) => Divider(
                    endIndent: 10,
                    indent: 10,
                    color: Colors.black,
                  ),
                  itemCount: _reasons.length,
                  itemBuilder: (context, index) {
                    print(_reasons);
                    return _buildReason(index, _reasons);
                  },
                ),
              ),
              RoundedLoadingButton(
                color: primaryColor,
                width: hBlock * 85,
                controller: _btnController,
                onPressed: () {
                  if (_reasons.length > 0) {
                    _postAddReason(context);
                  } else {
                    _btnController.reset();
                    _scaffoldKey.currentState.showSnackBar(SnackBar(
                      backgroundColor: Colors.red,
                      content: MyText.sText(
                          context: context,
                          text:
                              'Please_add_reason_and__'), 
                    ));
                  }
                },
                child: MyText.mText(
                    context: context,
                    text: "Save_reasons_of_rejection",
                    color: Colors.white),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildReason(int index, List<String> list) {
    return ListTile(
      title: MyText.mTextNL(context: context, text: list[index]),
      trailing: IconButton(
          icon: Icon(
            Icons.remove,
            color: Colors.red,
            size: hBlock * 10,
          ),
          onPressed: () {
            setState(() {
              list.removeAt(index);
            });
          }),
    );
  }

  _postAddReason(BuildContext context) async {
    Map<String, dynamic> jsonMap = {
      "reason": _reasons.first,
    };
    String jsonString = json.encode(jsonMap);
    ApiResponseModel responseModel = await NetworkUtil.post(
      'reasons',
      jsonString,
    );
    print(responseModel.message);
    if (responseModel == null) {
      return;
    }
    if (responseModel.statuscode == 200) {
      print('Reason added: ' + responseModel.message);

      //_btnController.success();
      setState(() {
        Navigator.of(context).pop();
      });
    } else {
      // _btnController.reset();

    }
  }
}
