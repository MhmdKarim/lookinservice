import 'package:flutter/material.dart';
import 'package:lookinservice/Generics/NetworkUtil.dart';
import 'package:lookinservice/Models/reasons_model.dart';
import 'package:lookinservice/PreDefined/app_constants.dart';
import 'package:lookinservice/PreDefined/localization.dart';
import 'package:lookinservice/Utils/size_config.dart';
import 'package:lookinservice/Views/Settings/add_reject_request_page.dart';
import 'package:lookinservice/Widgets/custom_appbar_widget.dart';
import 'package:lookinservice/Widgets/no_data_with_btn_widget.dart';
import 'package:lookinservice/Widgets/standard_button_widget.dart';
import 'package:lookinservice/Widgets/text_widgets.dart';

class RejectRequestReasons extends StatefulWidget {
  static const tag = '/RejectRequestReasons';

  @override
  _RejectRequestReasonsState createState() => _RejectRequestReasonsState();
}

class _RejectRequestReasonsState extends State<RejectRequestReasons> {
  AppLocalizations appLocalizations;
  bool _isLoading = false;
  List<ReasonsDataModel> _allReasons = [];

  @override
  void initState() {
   _getReasonsData();
    super.initState();
  }
  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig.init(context);
    appLocalizations = AppLocalizations.of(context);
    return Directionality(
      textDirection: isRightToLeft ? TextDirection.rtl : TextDirection.ltr,
      child: Scaffold(
        appBar: CustomAppBar.appBar(context, 'Reasons_of_rejection', null),
        body: _isLoading
            ? Center(child: CircularProgressIndicator())

            : 
            _allReasons.length == 0? 
            NoDataAvailableWithBtn(
              image: 'assets/reject-reasons-empty.png',
              text: 'No_added_reasons_available',
            ) :
            SingleChildScrollView(
                child: Column(
                  children: <Widget>[
                    Container(
                      height: vBlock * 70,
                      child: ListView.separated(
                        separatorBuilder: (context, index) => Divider(
                          endIndent: 10,
                          indent: 10,
                          color: Colors.black,
                        ),
                        itemCount: _allReasons.length,
                        itemBuilder: (context, index) {
                        //  print(_reasons);
                          return _buildReason(index, _allReasons);
                        },
                      ),
                    ),
                    Btn.button(
                        context: context,
                        title: 'add_and_update_reasons',
                        onPressed: () {
                          Navigator.of(context)
                              .pushNamed(AddRejectRequestReasons.tag);
                        })
                  ],
                ),
              ),
      ),
    );
  }

  Widget _buildReason(int index, List<ReasonsDataModel> list) {
    return ListTile(
      title: MyText.mTextNL(context: context, text: list[index].reason),
    );
  }

  _getReasonsData() async {
    _isLoading = true;
    ReasonsModel responseModel = await NetworkUtil.getReasons(
      'reasons',
    );
    if (responseModel == null) {
      return;
    }
    if (responseModel.statusCode == 200) {
      _allReasons = responseModel.data;
      if (_allReasons == null) {
        return;
      }

      if (mounted) {
        setState(() {
          _isLoading = false;
        });
      }
    } else {
      _isLoading = false;
    }
  }
}
