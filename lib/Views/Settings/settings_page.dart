import 'package:flutter/material.dart';
import 'package:lookinservice/Views/Settings/reject_request_page.dart';
import 'package:lookinservice/Widgets/custom_appbar_widget.dart';
import 'package:lookinservice/Widgets/icon_widget.dart';
import '../../PreDefined/app_constants.dart';
import '../../PreDefined/localization.dart';
import '../../Utils/size_config.dart';
import 'change_language_page.dart';
import 'terms_conditions_page.dart';
import '../../Widgets/text_widgets.dart';
import 'privacy_policy_page.dart';

class SettingsPage extends StatefulWidget {
  static const tag = '/SettingsPage';
  @override
  _MorePageState createState() => _MorePageState();
}

class _MorePageState extends State<SettingsPage> {
  AppLocalizations appLocalizations;
  bool isSwitched = false;

  @override
  Widget build(BuildContext context) {
    SizeConfig.init(context);
    appLocalizations = AppLocalizations.of(context);
    return Directionality(
      textDirection: isRightToLeft ? TextDirection.rtl : TextDirection.ltr,
      child: Scaffold(
        appBar: CustomAppBar.appBar(context, 'Settings',null),
        body: Container(
          child: ListView(
            children: <Widget>[
              _buildListTile(
                  context: context,
                  title: 'Reasons_of_rejection',
                  image: 'reject-reasons',
                  route: RejectRequestReasons.tag),
              _buildListTile(
                  context: context,
                  title: 'change_language',
                  image: 'change-language',
                  route: ChangeLanguagePage.tag),
              _buildListTile(
                  context: context,
                  title: 'terms_conditions',
                  image: 'terms',
                  route: TermsConditionsPage.tag),
              _buildListTile(
                  context: context,
                  title: 'privacy_policy',
                  image: 'privacy',
                  route: PrivacyPolicyPage.tag),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildListTile(
      {BuildContext context, String title, String image, String route}) {
    return ListTile(
      leading: CustomIcon(
        radius: 25,
        image: image,
        imageSize: 25
      ),
      title: MyText.mText(
        context: context,
        text: title,
      ),
      trailing: Icon(Icons.arrow_forward_ios,size: 20,),
      onTap: () {
        Navigator.pushNamed(context, route);
      },
    );
  }
}
